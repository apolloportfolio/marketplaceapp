// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    val kotlin_version = "1.9.0"
    val compose_version = "1.5.1"

    repositories {
        google()
        //jcenter() //Migration: https://jeroenmols.com/blog/2021/02/04/migratingjcenter/
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
    }
    dependencies {
        //        def hilt_version = "2.28-alpha"
        val hilt_version = "2.40.1"
        //        def hilt_version = "2.48"
        // last version without @Parcelize bug "non-static type variable T cannot be referenced from a static context"
        //        def hilt_version = "2.35.1" // https://mvnrepository.com/artifact/com.google.dagger/hilt-android-gradle-plugin
        classpath("com.android.tools.build:gradle:8.7.3")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:2.0.21")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hilt_version")
        classpath("com.google.gms:google-services:4.4.0")

        // https://firebase.google.com/docs/crashlytics/get-started?platform=android
        classpath("com.google.firebase:firebase-crashlytics-gradle:2.9.9")
    }
}

plugins {
    //    id("com.google.devtools.ksp") version "1.8.10-1.0.9" apply false
    id("com.google.devtools.ksp") version "2.0.21-1.0.27" apply false
    id("org.jetbrains.kotlin.plugin.compose") version "2.0.0" // this version matches your Kotlin version
    id("com.google.dagger.hilt.android") version "2.48" apply false
}

allprojects {
    repositories {
        google()
        //jcenter()
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
    }
}

tasks.register("clean") {
    delete(rootProject.buildDir)
}