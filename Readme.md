<div class="center-title">![Readme Screenshot](./screenshots/Readme.png)</div>

<div class="center-title">

# Marketplace App

</div>

<div class="container">

<div class="topleft-column">![Project Screenshot 1](./screenshots/image1.png)</div>

<div class="topcenter-column">

# Project Description

Introducing a cutting-edge Android application designed for the modern online marketplace. The application before you provides a seamless and user-friendly experience, allowing customers to browse and purchase products from a vast selection of vendors with ease. With advanced search filters, personalized recommendations, and secure payment options, users can shop with confidence and convenience. The app also offers a range of features such as order tracking, customer support, and in-app messaging, ensuring a seamless and enjoyable shopping experience from start to finish. 


</div>

<div class="topright-column">

# My Tech Stack:

*   Kotlin, Java
*   AndroidX
*   Material Design
*   Jetpack Compose
*   Coroutines & Flow
*   Android Architecture Components
*   Dagger Hilt
*   Gradle
*   GitLab
*   JUnit
*   Jupiter
*   Mockito
*   MockK
*   Espresso
*   Web Service Integration (REST, JSON, XML, GSON)
*   Firebase
*   Firestore
*   Room DB
*   REST
*   RESTful APIs
*   Retrofit2
*   GraphQL (Apollo)
*   Glide
*   Glide for Jetpack Compose
*   Coil for Jetpack Compose
*   Google Pay
*   Google Location
*   MVI
*   MVVM
*   MVC
*   Service Locator
*   Clean Architecture
*   Dependency Injection
*   CI/CD
*   Clean Code Principles
*   Agile Software Development Practices
*   Test Driven Development

</div>

</div>

<div class="container">

<div class="left-column">

# Target Customer Persona

The target user of this online marketplace Android application is tech-savvy and digitally inclined, typically falling between the ages of 18 to 45. They lead busy lifestyles and appreciate the convenience of online shopping, seeking to streamline their purchasing experience with quick and easy access to a wide variety of products. They value personalized recommendations, reliable customer service, and secure payment options. Whether they are looking for the latest fashion trends, home decor, or gadgets, this app provides a one-stop-shop for all their needs. They are confident in their purchasing decisions and trust this platform to provide them with the best shopping experience possible.


</div>

<div class="right-column">![Project Screenshot 2](./screenshots/image2.png)</div>

</div>

<div class="container">

<div class="left-column">![Project Screenshot 3](./screenshots/image3.png)</div>

<div class="right-column">

# Problem Statement

	1. Convenience: This online marketplace app solves the problem of convenience for users by providing a one-stop-shop for a vast selection of products from various vendors in one place, accessible anytime and anywhere through their Android devices.
	2. Time-saving: Users can save valuable time by using this app to browse, search, and purchase products quickly and easily, without the need to physically travel to multiple stores or websites.
	3. Personalized Recommendations: The app solves the problem of finding suitable products by providing personalized recommendations based on users' browsing and purchasing history, helping them discover new products they may be interested in.
	4. Secure Payment: The app provides secure payment options, ensuring that users can make transactions with confidence and without the risk of fraud or security breaches.
	5. Tracking and Monitoring: Users can track their orders through the app and monitor their progress, from purchase to delivery, providing them with a hassle-free experience.
	6. Customer Support: The app offers users reliable customer support, with easy access to assistance and support for any queries or issues they may have during their shopping experience.
	7. In-App Messaging: The app allows users to communicate with vendors directly through in-app messaging, solving the problem of communication barriers between buyers and sellers, enabling them to make informed purchasing decisions.


</div>

</div>

<div class="container">

<div class="right-column">

# Functionalities

	1. Search Functionality: This app allows users to search for specific products, vendors, or categories, using advanced filters such as price range, brand, and color, among others.
	2. Product Listing: The app displays product listings from various vendors, providing users with detailed information such as product descriptions, prices, reviews, ratings, and images.
	3. Cart and Checkout: The app enables users to add products to their cart, review their selections, and proceed to checkout, with the option to apply discount codes or vouchers.
	4. Payment Gateway Integration: The app integrates secure payment gateways, enabling users to make transactions with ease, including debit/credit cards, digital wallets, and other payment methods.
	5. Order Tracking and Delivery: The app provides users with real-time updates on their order status, from processing to dispatch to delivery, with options to track their orders through the app.
	6. Review and Rating System: The app includes a review and rating system, enabling users to share their experiences with products and vendors, helping other users make informed purchasing decisions.
	7. Personalized Recommendations: The app provides personalized recommendations based on users' browsing and purchasing history, suggesting products that match their interests and preferences.
	8. In-App Messaging: The app allows users to communicate with vendors directly through in-app messaging, enabling them to ask questions or request additional information about products.
	9. Customer Support: The app offers users reliable customer support, with options to contact customer service through the app for any queries or issues they may encounter during their shopping experience.
	10. User Profile: The app includes a user profile section, enabling users to view their order history, track their rewards, save their payment details, and manage their account settings.


</div>

<div class="left-column">![Project Screenshot 4](./screenshots/image4.png)</div>

</div>

<div class="container">

<div class="right-column">

# Application's Screens

	1. Home Screen: Displays personalized product recommendations, trending products, and featured collections, providing a quick and easy way for users to discover new products.
	2. Product Listing Screen: Displays product listings from various vendors, providing users with detailed information such as product descriptions, prices, reviews, ratings, and images.
	3. Product Detail Screen: Displays detailed information about a selected product, including images, product description, price, reviews, and ratings, among others.
	4. Cart Screen: Displays a list of selected products, enabling users to review their selections, apply discount codes or vouchers, and proceed to checkout.
	5. Checkout Screen: Allows users to review their order details, select a delivery option, and make payments using secure payment gateways.
	6. Order Confirmation Screen: Displays an order confirmation message, providing users with details of their order, including order number, delivery date, and tracking details.
	7. Order Tracking Screen: Displays real-time updates on order status, from processing to dispatch to delivery, with options to track orders through the app.
	8. Profile Screen: Allows users to view their order history, track their rewards, save their payment details, and manage their account settings.
	9. Settings Screen: Provides users with options to manage their app settings, including notification preferences, language, and other account-related settings.
	10. Customer Support Screen: Provides users with options to contact customer support through the app, enabling them to seek assistance or report any issues they may encounter.
	11. In-App Messaging Screen: Allows users to communicate with vendors directly through in-app messaging, enabling them to ask questions or request additional information about products.
	12. Search Screen: Provides users with advanced search options, allowing them to search for specific products, vendors, or categories, using filters such as price range, brand, size, and color, among others.
	13. Wishlist Screen: Allows users to save products for later, providing a convenient way to keep track of products they are interested in.


</div>

</div>

<div class="container">

<div class="right-column">

# Disclaimer

In order to provide their clients with protection of trade secrets, protection of user data, legal protection and competitive advantage all commissions of projects of this Developer are by default accompanied by an appropriate Non Disclosure Agreement (NDA). For the same reason all presented: project description, problem statement, functionalities and screen descriptions are for presentation purposes only and may or may not represent an entirety of project.

Due to NDA and obvious copyright and security issues, this GitHub project does not contain entire code of described application.

All included code snippets are for presentation purposes only and should not be used for commercial purposes as they are intellectual property of this application's Developer. In compliance with an appropriate NDA, all presented screenshots of working application have been made after removing any and all graphics that may be considered third party's intellectual property. As such they do not represent a final end product.

The developer does not claim any ownership or affiliation with any of the third-party libraries, technologies, or services used in the project.

While every effort has been made to ensure the accuracy, completeness, and reliability of the presented code, the developer cannot be held responsible for any errors, omissions, or damages that may arise from it's use.

In compliance with an appropriate NDA and in order to provide their clients with all presented: description, target customer persona, problem statement, functionalities, screen descriptions and other items are for presentation purposes only and do not consist an entirety of commisioned project.

Protection of trade secrets, protection of user data, legal protection and protection of competitive advantage of a Client is of utmost importance to the Developer.

</div>

</div>

<div class="container">

<div class="right-column">

# License

Any use of this code is prohibited except of for recruitment purposes only of the Developer who wrote it.

</div>

</div>