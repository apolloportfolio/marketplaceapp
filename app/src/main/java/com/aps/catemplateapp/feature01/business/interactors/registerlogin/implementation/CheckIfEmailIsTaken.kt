package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.CheckIfEmailIsTaken
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class CheckIfEmailIsTakenImpl @Inject constructor(
    private val networkDataSource: UserNetworkDataSource
): CheckIfEmailIsTaken {
    override fun checkIfEmailIsTaken(email: String?, stateEvent: StateEvent): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val cacheResult = safeCacheCall(Dispatchers.IO){
            networkDataSource.checkIfEmailExistsInRepository(email)
        }

        val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, Boolean?>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: Boolean?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                var message: String? =
                    SEARCH_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                var data = RegisterLoginActivityViewState<ProjectUser>()
                if(resultObj == null){
                    if(resultObj!!) {
                        message = EMAIL_TAKEN
                        data.emailIsTaken = true
                        uiComponentType = UIComponentType.Toast()
                    } else {
                        data.emailIsTaken = false
                    }
                } else {
                    message = EMAIL_NULL
                    data.emailIsTaken = true
                    uiComponentType = UIComponentType.Toast()
                }


                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = data,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    companion object{
        val EMAIL_TAKEN = "Email is taken."
        val EMAIL_NULL = "No response received from server. Try again later."
        val SEARCH_ENTITIES_SUCCESS = "Successfully found an email in repository."

    }
}