package com.aps.catemplateapp.feature01.business.interactors.registerlogin.implementation

import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.cache.CacheResponseHandler
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.data.util.safeCacheCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.abstraction.DeleteMultipleUsers
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DeleteMultipleUsersImpl
@Inject constructor (
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource
): DeleteMultipleUsers {

    // set true if an error occurs when deleting any of the notes from cache
    private var onDeleteError: Boolean = false

    override fun deleteEntities(
        entities: List<ProjectUser>,
        stateEvent: StateEvent
    ): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {

        val successfulDeletes: ArrayList<ProjectUser> = ArrayList() // notes that were successfully deleted
        for(entity in entities){
            val cacheResult = safeCacheCall(Dispatchers.IO){
                cacheDataSource.deleteEntity(entity.id)
            }

            val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, Int>(
                response = cacheResult,
                stateEvent = stateEvent
            ){
                override suspend fun handleSuccess(resultObj: Int): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                    if(resultObj < 0){ // if error
                        onDeleteError = true
                    }
                    else{
                        successfulDeletes.add(entity)
                    }
                    return null
                }
            }.getResult()

            // check for random errors
            if(response?.stateMessage?.response?.message
                    ?.contains(stateEvent.errorInfo()) == true){
                onDeleteError = true
            }

        }

        if(onDeleteError){
            emit(
                DataState.data<RegisterLoginActivityViewState<ProjectUser>>(
                    response = Response(
                        messageId = R.string.error,
                        message = DELETE_ENTITIES_ERRORS,
                        uiComponentType = UIComponentType.Dialog(),
                        messageType = MessageType.Success()
                    ),
                    data = null,
                    stateEvent = stateEvent
                )
            )
        }
        else{
            emit(
                DataState.data<RegisterLoginActivityViewState<ProjectUser>>(
                    response = Response(
                        messageId = R.string.error,
                        message = DELETE_ENTITIES_SUCCESS,
                        uiComponentType = UIComponentType.Toast(),
                        messageType = MessageType.Success()
                    ),
                    data = null,
                    stateEvent = stateEvent
                )
            )
        }

        updateNetwork(successfulDeletes)
    }

    private suspend fun updateNetwork(successfulDeletes: ArrayList<ProjectUser>){
        for (entity in successfulDeletes){

            // delete from "notes" node
            safeApiCall(Dispatchers.IO){
                networkDataSource.deleteEntity(entity.id)
            }

            // insert into "deletes" node
            safeApiCall(Dispatchers.IO){
                networkDataSource.insertDeletedEntity(entity)
            }
        }
    }

    companion object{
        val DELETE_ENTITIES_SUCCESS = "Successfully deleted entities."
        val DELETE_ENTITIES_ERRORS = "Not all the entities you selected were deleted. There was some errors."
        val DELETE_ENTITIES_YOU_MUST_SELECT = "You haven't selected any entities to delete."
        val DELETE_ENTITIES_ARE_YOU_SURE = "Are you sure you want to delete these?"
    }
}