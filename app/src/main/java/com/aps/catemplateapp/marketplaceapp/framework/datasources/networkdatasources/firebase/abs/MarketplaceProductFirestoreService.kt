package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs

import android.net.Uri
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers.MarketplaceProductFirestoreMapper
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceProductFirestoreEntity

interface MarketplaceProductFirestoreService: BasicFirestoreService<
        MarketplaceProduct,
        MarketplaceProductFirestoreEntity,
        MarketplaceProductFirestoreMapper
        > {
    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<MarketplaceProduct>?

    suspend fun getUsersMarketplaceProducts(userID: UserUniqueID): List<MarketplaceProduct>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : MarketplaceProduct,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}