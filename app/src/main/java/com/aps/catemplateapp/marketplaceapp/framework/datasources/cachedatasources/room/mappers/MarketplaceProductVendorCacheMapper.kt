package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductVendorCacheEntity
import javax.inject.Inject

class MarketplaceProductVendorCacheMapper
@Inject
constructor() : EntityMapper<MarketplaceProductVendorCacheEntity, MarketplaceProductVendor> {
    override fun mapFromEntity(entity: MarketplaceProductVendorCacheEntity): MarketplaceProductVendor {
        return MarketplaceProductVendor(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,
        )
    }

    override fun mapToEntity(domainModel: MarketplaceProductVendor): MarketplaceProductVendorCacheEntity {
        return MarketplaceProductVendorCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceProductVendorCacheEntity>) : List<MarketplaceProductVendor> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceProductVendor>): List<MarketplaceProductVendorCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}