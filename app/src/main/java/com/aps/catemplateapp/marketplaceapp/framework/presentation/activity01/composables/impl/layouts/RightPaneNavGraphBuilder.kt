package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.layouts

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.MarketplaceReview
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen2
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen3
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen4
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen5
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreenPlaceholder
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenStateEvent

@Composable
internal fun RightPaneNavGraphBuilder(
    launchStateEvent: (HomeScreenStateEvent) -> Unit,

    currentlyShownMarketplaceProduct: MarketplaceProduct?,
    marketplaceProducts: List<MarketplaceProduct>?,
    marketplaceProductsVendors: List<MarketplaceProductVendor>?,
    onVendorClick: (MarketplaceProductVendor) -> Unit,
    addMarketplaceProductToCart: (MarketplaceProduct) -> Unit,
    currentOrdersNumber: String?,
    currentOrdersDeliveryDate: String?,
    currentOrdersTrackingDetails: String?,
    navigateToHome: () -> Unit,
    currentlyShownMarketplaceProductsReviews: List<MarketplaceReview>?,
    onMarketplaceReviewClick: (MarketplaceReview) -> Unit,
    confirmOrder: () -> Unit,
    currentlyShownMarketplaceOrder: MarketplaceOrder?,

    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,
    stateEventTracker: StateEventTracker,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    card1CurrentlyShownEntity: MarketplaceProduct? = null,
    card1ActionOnEntity: (MarketplaceProduct?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,

    isPreview: Boolean = false,
): NavGraphBuilder.() -> Unit {
    return {
        composable(HomeScreenDestination.DetailsScreen1.route) {
            HomeScreenCompDetailsScreen1(
                launchStateEvent = launchStateEvent,

                currentlyShownMarketplaceProductsReviews = currentlyShownMarketplaceProductsReviews,
                onMarketplaceReviewClick = onMarketplaceReviewClick,

                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                entity = card1CurrentlyShownEntity,
                actionOnEntity = card1ActionOnEntity,
                navigateToProfileScreen = navigateToProfileScreen,
            )
        }
        composable(HomeScreenDestination.DetailsScreen2.route) {
            HomeScreenCompDetailsScreen2(
                launchStateEvent = launchStateEvent,

                marketplaceProducts = marketplaceProducts,
                vendors = marketplaceProductsVendors,
                onVendorClick = onVendorClick,
                addMarketplaceProductToCart = addMarketplaceProductToCart,

                activity = activity,
                permissionHandlingData = permissionHandlingData,

                stateEventTracker = stateEventTracker,

                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                currentlyShownMarketplaceProduct = currentlyShownMarketplaceProduct,
                navigateToProfileScreen = navigateToProfileScreen,
                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.DetailsScreen3.route) {
            HomeScreenCompDetailsScreen3(
                confirmOrder = confirmOrder,
                currentlyShownMarketplaceOrder = currentlyShownMarketplaceOrder,
                launchStateEvent = launchStateEvent,

                orderNumber = currentOrdersNumber,
                deliveryDate = currentOrdersDeliveryDate,
                trackingDetails = currentOrdersTrackingDetails,
                navigateToHome = navigateToHome,

                activity = activity,
                permissionHandlingData = permissionHandlingData,

                stateEventTracker = stateEventTracker,

                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                navigateToProfileScreen = navigateToProfileScreen,
                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.DetailsScreen4.route) {
            HomeScreenCompDetailsScreen4()
        }
        composable(HomeScreenDestination.DetailsScreen5.route) {
            HomeScreenCompDetailsScreen5()
        }
        composable(HomeScreenDestination.DetailsPlaceholderScreen.route) {
            HomeScreenCompDetailsScreenPlaceholder()
        }
    }
}