package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "marketplaceproduct")
data class MarketplaceProductCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "price")
    var price: Double?,

    @ColumnInfo(name = "currency")
    var currency: String?,

    @ColumnInfo(name = "brand")
    var brand: String?,

    @ColumnInfo(name = "productName")
    var productName: String?,

    @ColumnInfo(name = "picture2URI")
    var picture2URI: String?,

    @ColumnInfo(name = "picture3URI")
    var picture3URI: String?,

    @ColumnInfo(name = "picture4URI")
    var picture4URI: String?,

    @ColumnInfo(name = "picture5URI")
    var picture5URI: String?,

    @ColumnInfo(name = "picture6URI")
    var picture6URI: String?,

    @ColumnInfo(name = "picture7URI")
    var picture7URI: String?,

    @ColumnInfo(name = "picture8URI")
    var picture8URI: String?,

    @ColumnInfo(name = "picture9URI")
    var picture9URI: String?,

    @ColumnInfo(name = "picture10URI")
    var picture10URI: String?,

    @ColumnInfo(name = "productCategoryID")
    var productCategoryID: UniqueID?,

    @ColumnInfo(name = "productVendorID")
    var productVendorID: UniqueID?,

    @ColumnInfo(name = "latitude")
    var latitude : Double?,

    @ColumnInfo(name = "longitude")
    var longitude: Double?,

    @ColumnInfo(name = "geoLocation")
    var geoLocation: ParcelableGeoPoint?,

    @ColumnInfo(name = "firestoreGeoLocation")
    var firestoreGeoLocation: Double?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "city")
    var city : String?,

    @ColumnInfo(name = "ownerID")
    var ownerID: UserUniqueID?,

    @ColumnInfo(name = "name")
    var name : String?,

    @ColumnInfo(name = "switch1")
    var switch1 : Boolean? = null,

    @ColumnInfo(name = "switch2")
    var switch2 : Boolean? = null,

    @ColumnInfo(name = "switch3")
    var switch3 : Boolean? = null,

    @ColumnInfo(name = "switch4")
    var switch4 : Boolean? = null,

    @ColumnInfo(name = "switch5")
    var switch5 : Boolean? = null,

    @ColumnInfo(name = "switch6")
    var switch6 : Boolean? = null,

    @ColumnInfo(name = "switch7")
    var switch7 : Boolean? = null,
)
