package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers.MarketplaceProductCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Entity1DaoServiceImpl"
private const val LOG_ME = true

@Singleton
class MarketplaceProductDaoServiceImpl
@Inject
constructor(
    private val dao: MarketplaceProductDao,
    private val mapper: MarketplaceProductCacheMapper,
    private val dateUtil: DateUtil
): MarketplaceProductDaoService {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceProduct): MarketplaceProduct {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.price,
                entity.currency,
                entity.brand,
                entity.productName,
                entity.picture2URI,
                entity.picture3URI,
                entity.picture4URI,
                entity.picture5URI,
                entity.picture6URI,
                entity.picture7URI,
                entity.picture8URI,
                entity.picture9URI,
                entity.picture10URI,
                entity.productCategoryID,
                entity.productVendorID,

                entity.latitude,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,
                entity.description,

                entity.city,

                entity.ownerID,

                entity.name,

                entity.switch1,
                entity.switch2,
                entity.switch3,
                entity.switch4,
                entity.switch5,
                entity.switch6,
                entity.switch7,
            )
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: MarketplaceProduct): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<MarketplaceProduct>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProduct? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        price: Double?,
        currency: String?,
        brand: String?,
        productName: String?,
        picture2URI: String?,
        picture3URI: String?,
        picture4URI: String?,
        picture5URI: String?,
        picture6URI: String?,
        picture7URI: String?,
        picture8URI: String?,
        picture9URI: String?,
        picture10URI: String?,
        productCategoryID: UniqueID?,
        productVendorID: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                price,
                currency,
                brand,
                productName,
                picture2URI,
                picture3URI,
                picture4URI,
                picture5URI,
                picture6URI,
                picture7URI,
                picture8URI,
                picture9URI,
                picture10URI,
                productCategoryID,
                productVendorID,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                price,
                currency,
                brand,
                productName,
                picture2URI,
                picture3URI,
                picture4URI,
                picture5URI,
                picture6URI,
                picture7URI,
                picture8URI,
                picture9URI,
                picture10URI,
                productCategoryID,
                productVendorID,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceProduct>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<MarketplaceProduct> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<MarketplaceProduct> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}