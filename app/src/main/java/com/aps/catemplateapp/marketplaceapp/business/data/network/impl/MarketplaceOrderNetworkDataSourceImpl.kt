package com.aps.catemplateapp.marketplaceapp.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceOrderNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceOrderFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceOrderNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: MarketplaceOrderFirestoreService
): MarketplaceOrderNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceOrder): MarketplaceOrder? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: MarketplaceOrder) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<MarketplaceOrder>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: MarketplaceOrder) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<MarketplaceOrder> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: MarketplaceOrder): MarketplaceOrder? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<MarketplaceOrder> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<MarketplaceOrder>): List<MarketplaceOrder>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): MarketplaceOrder? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersMarketplaceOrders(userId : UserUniqueID) : List<MarketplaceOrder>? {
        return firestoreService.getUsersMarketplaceOrders(userId)
    }
}