package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Details
import androidx.compose.material.icons.filled.Numbers
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType07
import com.aps.catemplateapp.common.framework.presentation.views.RowWithButtonAndBackButton
import com.aps.catemplateapp.common.framework.presentation.views.RowWithLabelAndValueType01
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceOrderFactory
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsMarketplaceApp
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme


// HomeScreenCompDetailsScreen3
@Composable
fun MarketplaceAppOrderConfirmationScreenContent(
    orderNumber: String?,
    deliveryDate: String?,
    trackingDetails: String?,
    navigateToHome: () -> Unit,

    confirmOrder: () -> Unit,
    marketplaceOrder: MarketplaceOrder?,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        modifier = Modifier
            .fillMaxSize(),
        backgroundDrawableId = R.drawable.marketplace_app_background,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
    ) {
        if(marketplaceOrder == null) {
            TipContentIsUnavailable(tip = stringResource(id = R.string.error_null_data_in_order_confirmation_screen))
        } else {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(PaddingValues(4.dp, 3.dp, 4.dp, 3.dp))
            ) {

                val (
                    titleRow, orderDetails, orderContent, bottomButtons,
                ) = createRefs()


                val titleRowConstraint: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }

                val orderDetailsConstraint: ConstrainScope.() -> Unit = {
                    top.linkTo(titleRow.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }

                val orderContentConstraint: ConstrainScope.() -> Unit = {
                    top.linkTo(orderDetails.bottom)
                    bottom.linkTo(bottomButtons.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    height = Dimension.fillToConstraints
                }

                val bottomButtonsConstraint: ConstrainScope.() -> Unit = {
//                    top.linkTo(orderContent.bottom)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }

                TitleRowType01(
                    titleString = stringResource(id = R.string.order_confirmation_message),
                    textAlign = TextAlign.Center,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundTitleType01(
                        colors = listOf(
//                            MaterialTheme.colors.surface,
                            MaterialTheme.colors.primaryVariant,
                            MaterialTheme.colors.primary,
                        ),
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                    modifier = Modifier
                        .testTag("AppHeader")
                        .constrainAs(titleRow, titleRowConstraint),
                )

                if(orderNumber != null && deliveryDate != null && trackingDetails != null) {
                    Spacer(modifier = Modifier.fillMaxWidth().height(8.dp))

                    OrderDetails(
                        orderNumber,
                        deliveryDate,
                        trackingDetails,
                        modifier = Modifier
                            .testTag("OrderDetails")
                            .constrainAs(orderDetails, orderDetailsConstraint),
                    )
                } else {
                    TipContentIsUnavailable(
                        tip = stringResource(id = R.string.error_null_data_in_order_confirmation_screen),
                        modifier = Modifier
                            .height(100.dp)
                            .fillMaxWidth()
                            .constrainAs(orderDetails, orderDetailsConstraint)
                    )
                }

                OrderContent(
                    marketplaceOrder = marketplaceOrder,
                    modifier = Modifier
                        .fillMaxWidth()
                        .constrainAs(orderContent, orderContentConstraint),
                    isPreview = isPreview,
                )

                BottomButtons(
                    buttonOnClick = confirmOrder,
                    onBackButtonClick = navigateToHome,
                    modifier = Modifier
                        .constrainAs(bottomButtons, bottomButtonsConstraint)
                        .fillMaxWidth()
                        .wrapContentHeight()
                )

            }
        }

    }
}

@Composable
private fun OrderDetails(
    orderNumber: String,
    deliveryDate: String,
    trackingDetails: String,
    modifier: Modifier,
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(Dimens.columnPadding)
    ) {
        val cornerRadius = 8.dp
        RowWithLabelAndValueType01(
            icon = Icons.Default.Numbers,
            label = stringResource(id = R.string.order_number),
            value = orderNumber,
            tint = MaterialTheme.colors.onBackground,
            borderWidth = 2.dp,
            cornerRadius = cornerRadius,
            composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                colors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                ),
                brush = null,
                shape = RoundedCornerShape(cornerRadius),
                alpha = 0.7f,
            ),
        )

        Spacer(modifier = Modifier.fillMaxWidth().height(8.dp))

        RowWithLabelAndValueType01(
            icon = Icons.Default.DateRange,
            label = stringResource(id = R.string.delivery_date),
            value = deliveryDate,
            tint = MaterialTheme.colors.onBackground,
            borderWidth = 2.dp,
            cornerRadius = 8.dp,
            composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                colors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                ),
                brush = null,
                shape = null,
                alpha = 0.7f,
            ),
        )

        Spacer(modifier = Modifier.fillMaxWidth().height(8.dp))

        RowWithLabelAndValueType01(
            icon = Icons.Default.Details,
            label = stringResource(id = R.string.tracking_details),
            value = trackingDetails,
            tint = MaterialTheme.colors.onBackground,
            borderWidth = 2.dp,
            cornerRadius = 8.dp,
            composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                colors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                ),
                brush = null,
                shape = null,
                alpha = 0.7f,
            ),
        )

        Spacer(modifier = Modifier.fillMaxWidth().height(8.dp))
    }
}

@Composable
private fun BottomButtons(
    buttonOnClick: () -> Unit,
    onBackButtonClick: () -> Unit,
    modifier: Modifier,
) {
    RowWithButtonAndBackButton(
        buttonIcon = Icons.Default.Add,
        buttonString = stringResource(id = R.string.continue_shopping),
        buttonOnClick = buttonOnClick,
        buttonColor = MaterialTheme.colors.primary,
        buttonTextColor = MaterialTheme.colors.onPrimary,
        buttonTextStyle = TextStyle(
            fontFamily = FontFamily.Default,
            fontWeight = FontWeight.Bold,
            fontSize = 14.sp,
        ),
        onBackButtonClick = onBackButtonClick,
        internalPaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
            colors = listOf(
                MaterialTheme.colors.surface,
                MaterialTheme.colors.primary,
            ),
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        backgroundDrawableId = R.drawable.list_item_example_background_1,
        modifier = modifier,
    )
}

@Composable
private fun OrderContent(
    marketplaceOrder: MarketplaceOrder,
    modifier: Modifier,
    isPreview: Boolean = false,
) {
    val orderedProducts = marketplaceOrder.getOrderedProducts()
    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = R.drawable.list_item_example_background_1,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
        contentAlignment = Alignment.TopCenter
    ) {
        LazyColumn(
            Modifier
                .padding(Dimens.columnPadding)
                .fillMaxSize()
                .fillMaxWidth()
        ) {
            itemsIndexed(orderedProducts) { index, orderedProduct ->
                if(orderedProduct.productPriceAtOrder != null && orderedProduct.orderedAmount != null) {
                    val totalPrice: Double = 
                        orderedProduct.productPriceAtOrder!!.times(orderedProduct.orderedAmount!!)
                    val formattedTotalPrice: String = 
                        String.format("%.2f", totalPrice) + orderedProduct.currency
                    ListItemType07(
                        index = index,
                        itemTitleString = orderedProduct.productName,
                        itemDetail1String = orderedProduct.orderedAmount.toString(),
                        itemDetail2String = formattedTotalPrice,
                        onListItemClick = {},
                        itemRef = if(isPreview) { 
                            null 
                        } else {
                            MarketplaceOrder.getFirebasePictureRef(
                                orderedProduct.productId,
                                orderedProduct.pictureFirestoreUri,
                            )
                       },
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview(prefix = "marketplace_app_example_product_", suffix = index)
                        } else { null },
                        padding = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
                        backgroundDrawableId = R.drawable.list_item_example_background_1,
                        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        imageTint = null,
                    )
                }
            }
        }
    }
}

// Preview
@Preview
@Composable
private fun MarketplaceAppOrderConfirmationScreenContentPreview() {
    HomeScreenTheme {
        MarketplaceAppOrderConfirmationScreenContent(
            orderNumber = "123456",
            deliveryDate = "February 10, 2024",
            trackingDetails = "Tracking ID: 987654321",
            navigateToHome = {},
            confirmOrder = {},
            marketplaceOrder = MarketplaceOrderFactory.createPreviewEntitiesList()[0],
            isPreview = true,
        )
    }
}
