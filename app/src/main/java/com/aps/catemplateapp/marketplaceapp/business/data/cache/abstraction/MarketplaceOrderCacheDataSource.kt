package com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder

interface MarketplaceOrderCacheDataSource: StandardCacheDataSource<MarketplaceOrder> {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceOrder): MarketplaceOrder?

    override suspend fun insertEntity(entity: MarketplaceOrder): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<MarketplaceOrder>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,

        userId: UniqueID?,
        isPaid: Boolean?,
        isCanceled: Boolean?,
        orderedProducts: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceOrder>

    override suspend fun getAllEntities(): List<MarketplaceOrder>

    override suspend fun getEntityById(id: UniqueID?): MarketplaceOrder?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<MarketplaceOrder>): LongArray
}