package com.aps.catemplateapp.marketplaceapp.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.implementation.*
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceOrderDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductCategoryDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductVendorDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl.MarketplaceProductDaoServiceImpl
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl.MarketplaceOrderDaoServiceImpl
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl.MarketplaceProductCategoryDaoServiceImpl
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl.MarketplaceProductVendorDaoServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {

    @Binds
    abstract fun bindMarketplaceProductDaoService(implementation: MarketplaceProductDaoServiceImpl): MarketplaceProductDaoService

    @Binds
    abstract fun bindMarketplaceOrderDaoService(implementation: MarketplaceOrderDaoServiceImpl): MarketplaceOrderDaoService

    @Binds
    abstract fun bindMarketplaceProductCategoryDaoService(implementation: MarketplaceProductCategoryDaoServiceImpl): MarketplaceProductCategoryDaoService

    @Binds
    abstract fun bindMarketplaceProductVendorDaoService(implementation: MarketplaceProductVendorDaoServiceImpl): MarketplaceProductVendorDaoService

}