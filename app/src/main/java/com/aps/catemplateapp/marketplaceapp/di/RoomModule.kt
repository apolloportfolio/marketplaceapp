package com.aps.catemplateapp.marketplaceapp.di

import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceOrderDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductCategoryDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductVendorDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideMarketplaceProductDao(database : ProjectRoomDatabase) : MarketplaceProductDao {
        return database.marketplaceProductDao()
    }

    @Singleton
    @Provides
    fun provideMarketplaceOrderDao(database : ProjectRoomDatabase) : MarketplaceOrderDao {
        return database.marketplaceOrderDao()
    }

    @Singleton
    @Provides
    fun provideMarketplaceProductCategoryDao(database : ProjectRoomDatabase) : MarketplaceProductCategoryDao {
        return database.marketplaceProductCategoryDao()
    }

    @Singleton
    @Provides
    fun provideMarketplaceProductVendorDao(database : ProjectRoomDatabase) : MarketplaceProductVendorDao {
        return database.marketplaceProductVendorDao()
    }
}