package com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCategoryCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductCategoryDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductCategoryCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: MarketplaceProductCategoryDaoService
): MarketplaceProductCategoryCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: MarketplaceProductCategory): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceProductCategory>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,
        parentId: UniqueID?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,
            parentId,

            picture1URI,

            name,
            description,

            ownerID,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceProductCategory> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<MarketplaceProductCategory> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProductCategory? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<MarketplaceProductCategory>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}