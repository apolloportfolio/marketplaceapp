package com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductVendorCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductVendorDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductVendorCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: MarketplaceProductVendorDaoService
): MarketplaceProductVendorCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: MarketplaceProductVendor): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceProductVendor>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceProductVendor> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<MarketplaceProductVendor> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProductVendor? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<MarketplaceProductVendor>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}