package com.aps.catemplateapp.marketplaceapp.business.interactors

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.marketplaceapp.business.interactors.abs.*
import javax.inject.Inject

// Use cases
class HomeScreenInteractors<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.catemplateapp.common.business.domain.state.ViewState<Entity>>
@Inject
constructor(
    val getMarketplaceProductsAroundUser: GetMarketplaceProductsAroundUser,
    val searchMarketplaceProducts: SearchMarketplaceProducts,
    val getMarketplaceOrders: GetMarketplaceOrders,
    val getMarketplaceProductCategorys: GetMarketplaceProductCategorys,
    val getUsersRating: GetUsersRating,
    val logout: LogoutUser,
    val checkGooglePayAvailability: CheckGooglePayAvailability,
    val downloadExchangeRates: DownloadExchangeRates,
    val getMerchantName: GetMerchantName,
    val getGatewayNameAndMerchantID: GetGatewayNameAndMerchantID,

    val doNothingAtAll: DoNothingAtAll<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
)