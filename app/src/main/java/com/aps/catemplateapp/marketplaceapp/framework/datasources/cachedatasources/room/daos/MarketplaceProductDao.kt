package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductCacheEntity
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductVendorCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface MarketplaceProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : MarketplaceProductCacheEntity) : Long

    @Query("SELECT * FROM marketplaceproduct")
    suspend fun get() : List<MarketplaceProductCacheEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<MarketplaceProductCacheEntity>): LongArray

    @Query("SELECT * FROM marketplaceproduct WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): MarketplaceProductCacheEntity?

    @Query("DELETE FROM marketplaceproduct WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM marketplaceproduct")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM marketplaceproduct")
    suspend fun getAllEntities(): List<MarketplaceProductCacheEntity>

    @Query(
        """
        UPDATE marketplaceproduct 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        price = :price,
        currency = :currency,
        brand = :brand,
        productName = :productName,
        picture2URI = :picture2URI,
        picture3URI = :picture3URI,
        picture4URI = :picture4URI,
        picture5URI = :picture5URI,
        picture6URI = :picture6URI,
        picture7URI = :picture7URI,
        picture8URI = :picture8URI,
        picture9URI = :picture9URI,
        picture10URI = :picture10URI,
        productCategoryID = :productCategoryID,
        productVendorID = :productVendorID,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        city = :city,
        ownerID = :ownerID,
        name = :name,
        switch1 = :switch1,
        switch2 = :switch2,
        switch3 = :switch3,
        switch4 = :switch4,
        switch5 = :switch5,
        switch6 = :switch6,
        switch7 = :switch7
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        price: Double?,
        currency: String?,
        brand: String?,
        productName: String?,
        picture2URI: String?,
        picture3URI: String?,
        picture4URI: String?,
        picture5URI: String?,
        picture6URI: String?,
        picture7URI: String?,
        picture8URI: String?,
        picture9URI: String?,
        picture10URI: String?,
        productCategoryID: UniqueID?,
        productVendorID: UniqueID?,

        latitude: Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int

    @Query("DELETE FROM marketplaceproduct WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM marketplaceproduct")
    suspend fun searchEntities(): List<MarketplaceProductCacheEntity>

    @Query("SELECT COUNT(*) FROM marketplaceproduct")
    suspend fun getNumEntities(): Int
}