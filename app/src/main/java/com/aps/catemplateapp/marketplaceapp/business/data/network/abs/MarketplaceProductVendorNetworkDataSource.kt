package com.aps.catemplateapp.marketplaceapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor

interface MarketplaceProductVendorNetworkDataSource: StandardNetworkDataSource<MarketplaceProductVendor> {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: MarketplaceProductVendor)

    override suspend fun insertDeletedEntities(Entities: List<MarketplaceProductVendor>)

    override suspend fun deleteDeletedEntity(entity: MarketplaceProductVendor)

    override suspend fun getDeletedEntities(): List<MarketplaceProductVendor>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor?

    override suspend fun getAllEntities(): List<MarketplaceProductVendor>

    override suspend fun insertOrUpdateEntities(Entities: List<MarketplaceProductVendor>): List<MarketplaceProductVendor>?

    override suspend fun getEntityById(id: UniqueID): MarketplaceProductVendor?
}