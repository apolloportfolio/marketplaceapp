package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductVendorDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductVendorDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers.MarketplaceProductVendorCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Entity4DaoServiceImpl"
private const val LOG_ME = true

@Singleton
class MarketplaceProductVendorDaoServiceImpl
@Inject
constructor(
    private val dao: MarketplaceProductVendorDao,
    private val mapper: MarketplaceProductVendorCacheMapper,
    private val dateUtil: DateUtil
): MarketplaceProductVendorDaoService {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,
                entity.name,
                entity.description,
            )
                val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: MarketplaceProductVendor): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<MarketplaceProductVendor>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProductVendor? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                name,
                description,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                name,
                description,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceProductVendor>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<MarketplaceProductVendor> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<MarketplaceProductVendor> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}