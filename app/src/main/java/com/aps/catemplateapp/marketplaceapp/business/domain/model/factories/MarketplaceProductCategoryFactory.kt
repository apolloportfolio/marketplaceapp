package com.aps.catemplateapp.marketplaceapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductCategoryFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        parentId: UniqueID?,

        picture1URI: String?,
        name: String?,
        description : String?,

        ownerID: UserUniqueID?,
    ): MarketplaceProductCategory {
        return MarketplaceProductCategory(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            parentId,

            picture1URI,
            name,
            description,

            ownerID,
        )
    }

    fun createEntitiesList(numEntities: Int): List<MarketplaceProductCategory> {
        val list: ArrayList<MarketplaceProductCategory> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): MarketplaceProductCategory {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<MarketplaceProductCategory> {
            return arrayListOf(
                MarketplaceProductCategory(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent01"),
                    picture1URI = "1",
                    name = "test01",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent02"),
                    picture1URI = "1",
                    name = "test02",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent03"),
                    picture1URI = "1",
                    name = "test03",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent04"),
                    picture1URI = "1",
                    name = "test04",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent05"),
                    picture1URI = "1",
                    name = "test05",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent06"),
                    picture1URI = "1",
                    name = "test06",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent07"),
                    picture1URI = "1",
                    name = "test07",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent08"),
                    picture1URI = "1",
                    name = "test08",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent09"),
                    picture1URI = "1",
                    name = "test09",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceProductCategory(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    parentId = UniqueID("parent10"),
                    picture1URI = "1",
                    name = "test10",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                )
            )
        }
    }
}