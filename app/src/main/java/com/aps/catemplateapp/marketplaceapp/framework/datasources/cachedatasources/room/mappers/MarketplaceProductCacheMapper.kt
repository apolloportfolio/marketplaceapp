package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductCacheEntity
import javax.inject.Inject

class MarketplaceProductCacheMapper
@Inject
constructor() : EntityMapper<MarketplaceProductCacheEntity, MarketplaceProduct> {
    override fun mapFromEntity(entity: MarketplaceProductCacheEntity): MarketplaceProduct {
        return MarketplaceProduct(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.price,
            entity.currency,
            entity.brand,
            entity.productName,
            entity.picture2URI,
            entity.picture3URI,
            entity.picture4URI,
            entity.picture5URI,
            entity.picture6URI,
            entity.picture7URI,
            entity.picture8URI,
            entity.picture9URI,
            entity.picture10URI,
            entity.productCategoryID,
            entity.productVendorID,


            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
        )
    }

    override fun mapToEntity(domainModel: MarketplaceProduct): MarketplaceProductCacheEntity {
        return MarketplaceProductCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.price,
            domainModel.currency,
            domainModel.brand,
            domainModel.productName,
            domainModel.picture2URI,
            domainModel.picture3URI,
            domainModel.picture4URI,
            domainModel.picture5URI,
            domainModel.picture6URI,
            domainModel.picture7URI,
            domainModel.picture8URI,
            domainModel.picture9URI,
            domainModel.picture10URI,
            domainModel.productCategoryID,
            domainModel.productVendorID,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.city,

            domainModel.ownerID,

            domainModel.name,

            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceProductCacheEntity>) : List<MarketplaceProduct> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceProduct>): List<MarketplaceProductCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}