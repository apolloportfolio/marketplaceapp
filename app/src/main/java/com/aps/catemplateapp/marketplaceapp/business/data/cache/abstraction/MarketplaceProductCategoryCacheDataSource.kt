package com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction

import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory

interface MarketplaceProductCategoryCacheDataSource: StandardCacheDataSource<MarketplaceProductCategory> {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory?

    override suspend fun insertEntity(entity: MarketplaceProductCategory): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<MarketplaceProductCategory>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,
        parentId: UniqueID?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceProductCategory>

    override suspend fun getAllEntities(): List<MarketplaceProductCategory>

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProductCategory?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<MarketplaceProductCategory>): LongArray
}