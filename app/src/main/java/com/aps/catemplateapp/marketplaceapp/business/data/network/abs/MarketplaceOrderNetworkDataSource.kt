package com.aps.catemplateapp.marketplaceapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder

interface MarketplaceOrderNetworkDataSource: StandardNetworkDataSource<MarketplaceOrder> {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceOrder): MarketplaceOrder?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: MarketplaceOrder)

    override suspend fun insertDeletedEntities(Entities: List<MarketplaceOrder>)

    override suspend fun deleteDeletedEntity(entity: MarketplaceOrder)

    override suspend fun getDeletedEntities(): List<MarketplaceOrder>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: MarketplaceOrder): MarketplaceOrder?

    override suspend fun getAllEntities(): List<MarketplaceOrder>

    override suspend fun insertOrUpdateEntities(Entities: List<MarketplaceOrder>): List<MarketplaceOrder>?

    override suspend fun getEntityById(id: UniqueID): MarketplaceOrder?

    suspend fun getUsersMarketplaceOrders(userId : UserUniqueID) : List<MarketplaceOrder>?
}