package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.firebase.firestore.GeoPoint
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

private const val TAG = "MarketplaceProductFirestoreEntity"
private const val LOG_ME = true

@Entity(tableName = "entities1")
data class MarketplaceProductFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("price")
    @Expose
    var price: Double?,

    @SerializedName("currency")
    @Expose
    var currency: String?,

    @SerializedName("brand")
    @Expose
    var brand: String?,

    @SerializedName("productName")
    @Expose
    var productName: String?,

    @SerializedName("picture2URI")
    @Expose
    var picture2URI: String?,

    @SerializedName("picture3URI")
    @Expose
    var picture3URI: String?,

    @SerializedName("picture4URI")
    @Expose
    var picture4URI: String?,

    @SerializedName("picture5URI")
    @Expose
    var picture5URI: String?,

    @SerializedName("picture6URI")
    @Expose
    var picture6URI: String?,

    @SerializedName("picture7URI")
    @Expose
    var picture7URI: String?,

    @SerializedName("picture8URI")
    @Expose
    var picture8URI: String?,

    @SerializedName("picture9URI")
    @Expose
    var picture9URI: String?,

    @SerializedName("picture10URI")
    @Expose
    var picture10URI: String?,

    @SerializedName("productCategoryID")
    @Expose
    var productCategoryID: UniqueID?,

    @SerializedName("productVendorID")
    @Expose
    var productVendorID: UniqueID?,

    @SerializedName("lattitude")
    @Expose
    var lattitude : Double?,

    @SerializedName("longitude")
    @Expose
    var longitude: Double?,

    @SerializedName("geoLocation")
    @Expose
    var geoLocation: GeoPoint?,

    @SerializedName("firestoreGeoLocation")
    @Expose
    var firestoreGeoLocation: Double?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("description")
    @Expose
    var description : String?,

    @SerializedName("city")
    @Expose
    var city : String?,

    @SerializedName("keywords")
    @Expose
    var keywords : List<String>? = null,

    @SerializedName("ownerID")
    @Expose
    var ownerID: UserUniqueID?,

    @SerializedName("name")
    @Expose
    var name : String?,

    @SerializedName("switch1")
    @Expose
    var switch1 : Boolean?,

    @SerializedName("switch2")
    @Expose
    var switch2 : Boolean?,

    @SerializedName("switch3")
    @Expose
    var switch3 : Boolean?,

    @SerializedName("switch4")
    @Expose
    var switch4 : Boolean?,

    @SerializedName("switch5")
    @Expose
    var switch5 : Boolean?,

    @SerializedName("switch6")
    @Expose
    var switch6 : Boolean?,

    @SerializedName("switch7")
    @Expose
    var switch7 : Boolean?,

    ) {

    // Method generates keywords for searching entities in Firestore
    // https://medium.com/flobiz-blog/full-text-search-with-firestore-on-android-622af6ca5410
    fun generateKeywords(): MarketplaceProductFirestoreEntity {
        val methodName: String = "generateKeywords"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val generatedKeywords = mutableListOf<String>()

            this.city?.let {
                for (i in 0 until city!!.length) {
                    for (j in (i+1)..city!!.length) {
                        generatedKeywords.add(city!!.slice(i until j).toLowerCase() + " ")
                    }
                }
            }

            keywords = generatedKeywords
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return this
    }

    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}