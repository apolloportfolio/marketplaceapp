package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType03
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceOrderFactory
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompFragment2"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment2(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    entities: List<MarketplaceOrder>?,
    onListItemClick: (MarketplaceOrder) -> Unit,
    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},
    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment2BottomSheetActions,
) {
    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetEntities2StateEvent
            )
        } else {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = mutableSetOf(),
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        sheetState = sheetState,
        progressIndicatorState = progressIndicatorState,
        sheetContent = HomeScreenCompFragment2BottomSheet(
            scope = scope,
            sheetState = sheetState,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
        ),
        content = {
            HomeScreenComposableFragment2Content(
                stateEventTracker = stateEventTracker,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                entities = entities,
                onListItemClick = onListItemClick,
                launchInitStateEvent = launchInitStateEvent,
            )
        }
    )
}

@Composable
internal fun HomeScreenComposableFragment2Content(
    stateEventTracker: StateEventTracker,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    entities: List<MarketplaceOrder>?,
    onListItemClick: (MarketplaceOrder) -> Unit,
    launchInitStateEvent: () -> Unit,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.default_background
    ) {
        val fragmentsContentDescription = stringResource(id = R.string.home_screen_card2_content_description)
        Column(
            modifier = Modifier
                .semantics {
                    contentDescription = fragmentsContentDescription
                },
            verticalArrangement = Arrangement.Center,
        ) {
            if(showProfileStatusBar) ProfileStatusBar(finishVerification)

            if(entities?.isNotEmpty() == true) {
                LazyColumnWithPullToRefresh(
                    onRefresh = launchInitStateEvent,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(entities) { index, item ->
                        ListItemType03(
                            index = index,
                            itemTitleString = stringResource(id = R.string.item_name_placeholder),
                            itemPriceString = "",
                            itemLikesString = "",
                            itemDistanceString = "",
                            lblSmallShortAttribute3String = "",
                            itemDescriptionString = item.description,
                            onListItemClick = { onListItemClick(item) },
                            getItemsRating = { null },
                            itemRef = item.picture1FirebaseImageRef,
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment2_list_is_empty_tip))
            }
        }
    }

}




// Preview =========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment2Preview(
    @PreviewParameter(
        HomeScreenComposableFragment2ParamsProvider::class
    ) params: HomeScreenComposableFragment2Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment2BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompFragment2(
            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            entities = params.entities,
            onListItemClick = params.onListItemClick,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            bottomSheetActions = bottomSheetActions,
        )
    }
}

class HomeScreenComposableFragment2ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment2Params> {
    override val values: Sequence<HomeScreenComposableFragment2Params> = sequenceOf(
        HomeScreenComposableFragment2Params(
            deviceLocation = DeviceLocation(
                locationPermissionGranted = true,
                location = Location("Lublin").apply {
                    latitude = 51.2465
                    longitude = 22.5684
                }
            ),
            showProfileStatusBar = true,
            finishVerification = {},
            entities = MarketplaceOrderFactory.createPreviewEntitiesList(),
            onListItemClick = {},
            onSwipeLeft = {},
            onSwipeRight = {},
            onSwipeUp = {},
            onSwipeDown = {},
            floatingActionButtonDrawableId = R.drawable.ic_search_yellow_24,
            floatingActionButtonOnClick = { },
            floatingActionButtonContentDescription = "FAB 2",
        )
    )
}

data class HomeScreenComposableFragment2Params(
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val entities: List<MarketplaceOrder>?,
    val onListItemClick: (MarketplaceOrder) -> Unit,
    val onSwipeLeft: () -> Unit,
    val onSwipeRight: () -> Unit,
    val onSwipeUp: () -> Unit,
    val onSwipeDown: () -> Unit,
    val floatingActionButtonDrawableId: Int?,
    val floatingActionButtonOnClick: (() -> Unit)?,
    val floatingActionButtonContentDescription: String?,
)