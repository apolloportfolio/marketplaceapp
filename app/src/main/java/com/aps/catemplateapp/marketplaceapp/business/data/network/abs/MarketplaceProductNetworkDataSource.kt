package com.aps.catemplateapp.marketplaceapp.business.data.network.abs

import android.net.Uri
import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.interactors.impl.FirestoreEntity1SearchParameters

interface MarketplaceProductNetworkDataSource: StandardNetworkDataSource<MarketplaceProduct> {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProduct): MarketplaceProduct?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: MarketplaceProduct)

    override suspend fun insertDeletedEntities(Entities: List<MarketplaceProduct>)

    override suspend fun deleteDeletedEntity(entity: MarketplaceProduct)

    override suspend fun getDeletedEntities(): List<MarketplaceProduct>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: MarketplaceProduct): MarketplaceProduct?

    override suspend fun getAllEntities(): List<MarketplaceProduct>

    override suspend fun insertOrUpdateEntities(Entities: List<MarketplaceProduct>): List<MarketplaceProduct>?

    override suspend fun getEntityById(id: UniqueID): MarketplaceProduct?

    suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<MarketplaceProduct>?

    suspend fun getUsersMarketplaceProducts(userID: UserUniqueID) : List<MarketplaceProduct>?

    suspend fun uploadMarketplaceProductsPhotoToFirestore(
        entity : MarketplaceProduct,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}