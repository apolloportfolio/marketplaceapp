package com.aps.catemplateapp.marketplaceapp.business.interactors.abs

import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface GetMarketplaceOrders {
    fun getMarketplaceOrders(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<MarketplaceOrder>?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit = {},
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}