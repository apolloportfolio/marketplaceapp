package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory

interface MarketplaceProductCategoryDaoService {
    suspend fun insertOrUpdateEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory

    suspend fun insertEntity(entity: MarketplaceProductCategory): Long

    suspend fun insertEntities(Entities: List<MarketplaceProductCategory>): LongArray

    suspend fun getEntityById(id: UniqueID?): MarketplaceProductCategory?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,
        parentId: UniqueID?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<MarketplaceProductCategory>): Int

    suspend fun searchEntities(): List<MarketplaceProductCategory>

    suspend fun getAllEntities(): List<MarketplaceProductCategory>

    suspend fun getNumEntities(): Int
}