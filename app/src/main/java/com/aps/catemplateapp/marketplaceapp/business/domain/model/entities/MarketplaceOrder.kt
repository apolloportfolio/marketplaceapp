package com.aps.catemplateapp.marketplaceapp.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.IgnoredOnParcel
import java.io.Serializable


// Entity2
@Parcelize
data class MarketplaceOrder(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,

    var userId: UniqueID?,
    var isPaid: Boolean?,
    var isCanceled: Boolean?,
    var orderedProducts: String?,

    var picture1URI: String?,

    var name: String?,
    var description: String?,

    var ownerID: UserUniqueID?,
): Parcelable, Serializable {

    @IgnoredOnParcel
    val picture1FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_2_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture1URI!!)
            } else {
                null
            }
        }


    fun toIdString() : String {
        return "${this.javaClass.simpleName}($id)"
    }

    fun getOrderedProducts(): List<OrderedProduct> {
        val productList = mutableListOf<OrderedProduct>()
        this.orderedProducts?.let {
            val productDetails = it.split(", ")

            for (i in productDetails.indices step 6) {
                val productName = productDetails[i]
                val productId = UniqueID(productDetails[i + 1])
                val orderedAmount = productDetails[i + 2].toIntOrNull()
                val productPriceAtOrder = productDetails[i + 3].toDoubleOrNull()
                val currency = productDetails[i + 4]
                val pictureFirestoreUri = productDetails[i + 5]

                val orderedProduct = OrderedProduct(
                    productName,
                    productId,
                    orderedAmount,
                    productPriceAtOrder,
                    currency,
                    pictureFirestoreUri
                )
                productList.add(orderedProduct)
            }
        }

        return productList
    }

    companion object {
        const val ownerIDFieldName = "ownerID"

        fun getFirebasePictureRef(
            id: UniqueID?,
            pictureUri: String?,
        ): StorageReference? {
            return if(id != null && pictureUri != null) {
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_2_IMAGES_SUB_COLLECTION)
                    .child(id.firestoreDocumentID)
                    .child(pictureUri)
            } else {
                null
            }
        }
    }

    data class OrderedProduct(
        var productName: String?,
        var productId: UniqueID?,
        var orderedAmount: Int?,
        var productPriceAtOrder: Double?,
        var currency: String?,
        var pictureFirestoreUri: String?,
    )
}