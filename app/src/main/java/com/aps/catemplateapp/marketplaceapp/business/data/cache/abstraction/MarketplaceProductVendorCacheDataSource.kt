package com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor

interface MarketplaceProductVendorCacheDataSource: StandardCacheDataSource<MarketplaceProductVendor> {
    override suspend fun insertEntity(entity: MarketplaceProductVendor): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<MarketplaceProductVendor>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceProductVendor>

    override suspend fun getAllEntities(): List<MarketplaceProductVendor>

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProductVendor?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<MarketplaceProductVendor>): LongArray
}