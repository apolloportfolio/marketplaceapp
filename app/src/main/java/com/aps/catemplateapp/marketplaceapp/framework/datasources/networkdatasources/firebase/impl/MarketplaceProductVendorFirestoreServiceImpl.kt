package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductVendorFirestoreService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers.MarketplaceProductVendorFirestoreMapper
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceProductVendorFirestoreEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MarketplaceProductVendorFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: MarketplaceProductVendorFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): MarketplaceProductVendorFirestoreService,
    BasicFirestoreServiceImpl<MarketplaceProductVendor, MarketplaceProductVendorFirestoreEntity, MarketplaceProductVendorFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: MarketplaceProductVendor, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: MarketplaceProductVendor): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: MarketplaceProductVendorFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: MarketplaceProductVendor, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: MarketplaceProductVendorFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<MarketplaceProductVendorFirestoreEntity> {
        return MarketplaceProductVendorFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: MarketplaceProductVendorFirestoreEntity): UniqueID? {
        return entity.id
    }

    companion object {
        const val TAG = "Entity4FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity4"
        const val DELETES_COLLECTION_NAME = "entity4_d"
    }
}