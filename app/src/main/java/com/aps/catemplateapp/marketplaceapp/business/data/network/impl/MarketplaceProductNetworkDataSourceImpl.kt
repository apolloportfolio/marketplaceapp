package com.aps.catemplateapp.marketplaceapp.business.data.network.impl

import android.net.Uri
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.interactors.impl.FirestoreEntity1SearchParameters
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductFirestoreService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MarketplaceProductNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: MarketplaceProductFirestoreService
): MarketplaceProductNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProduct): MarketplaceProduct? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: MarketplaceProduct) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<MarketplaceProduct>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: MarketplaceProduct) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<MarketplaceProduct> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: MarketplaceProduct): MarketplaceProduct? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<MarketplaceProduct> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<MarketplaceProduct>): List<MarketplaceProduct>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): MarketplaceProduct? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreEntity1SearchParameters
    ) : List<MarketplaceProduct>? {
        if(LOG_ME) ALog.d(TAG, ".searchEntities(): searchParameters:\n$searchParameters")
        return firestoreService.searchEntities(searchParameters)
    }

    override suspend fun getUsersMarketplaceProducts(userID: UserUniqueID) : List<MarketplaceProduct>? {
        return firestoreService.getUsersMarketplaceProducts(userID)
    }

    override suspend fun uploadMarketplaceProductsPhotoToFirestore(
        entity : MarketplaceProduct,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        return firestoreService.uploadEntity1PhotoToFirestore(
            entity,
            entitysPhotoUri,
            entitysPhotoNumber
        )
    }

    companion object {
        const val TAG = "Entity1NetworkDataSourceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
