package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceOrderCacheEntity
import javax.inject.Inject

class MarketplaceOrderCacheMapper
@Inject
constructor() : EntityMapper<MarketplaceOrderCacheEntity, MarketplaceOrder> {
    override fun mapFromEntity(entity: MarketplaceOrderCacheEntity): MarketplaceOrder {
        return MarketplaceOrder(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.userId,
            entity.isPaid,
            entity.isCanceled,
            entity.orderedProducts,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: MarketplaceOrder): MarketplaceOrderCacheEntity {
        return MarketplaceOrderCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.userId,
            domainModel.isPaid,
            domainModel.isCanceled,
            domainModel.orderedProducts,

            domainModel.picture1URI,

            domainModel.name,
            domainModel.description,

            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceOrderCacheEntity>) : List<MarketplaceOrder> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceOrder>): List<MarketplaceOrderCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}