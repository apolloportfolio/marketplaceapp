package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.tooling.preview.Preview
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.PermissionHandlingData
import com.aps.catemplateapp.common.framework.presentation.views.DialogState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.catemplateapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.catemplateapp.common.framework.presentation.views.SnackBarState
import com.aps.catemplateapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceOrderFactory
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompDetailsScreen3"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompDetailsScreen3(
    confirmOrder: () -> Unit,
    currentlyShownMarketplaceOrder: MarketplaceOrder?,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    orderNumber: String?,
    deliveryDate: String?,
    trackingDetails: String?,
    navigateToHome: () -> Unit,

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    navigateToProfileScreen: () -> Unit,
    isPreview: Boolean = false,
) {
    val permissionsRequiredInFragment = mutableSetOf<String>()

    val scope = rememberCoroutineScope()            // For bottom sheet if implemented.
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model. No state event neccesary.")
        } else {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = {},
        onSwipeRight = {},
        onSwipeUp = {},
        onSwipeDown = {},
        onSwipeRightFromComposableSide = {},
        onSwipeLeftFromComposableSide = {},
        onSwipeDownFromComposableSide = {},
        onSwipeUpFromComposableSide = {},
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = null,
        floatingActionButtonOnClick = {},
        floatingActionButtonContentDescription = null,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = {},
        content = {

            MarketplaceAppOrderConfirmationScreenContent(
                orderNumber = orderNumber,
                deliveryDate = deliveryDate,
                trackingDetails = trackingDetails,
                navigateToHome = navigateToHome,
                confirmOrder = confirmOrder,
                marketplaceOrder = currentlyShownMarketplaceOrder,
            )

        },
        isPreview = isPreview,
    )
}

//========================================================================================
@Preview
@Composable
fun HomeScreenComposableDetailsScreen3Preview() {
    val previewEntity = MarketplaceProductFactory.createPreviewMarketplaceProductsList()[0]
    val navigateToProfileScreen = {}
    val actionOnEntity: (MarketplaceProduct?, ()-> Unit) -> Unit = { _, _ -> }
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast from Preview!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompDetailsScreen3(
            confirmOrder = {},
            currentlyShownMarketplaceOrder = MarketplaceOrderFactory.createPreviewEntitiesList()[0],
            orderNumber = "123456789",
            deliveryDate = "2023-02-10 08:30:00",
            trackingDetails = "ABC123456789",
            navigateToHome = {},

            stateEventTracker = StateEventTracker(),
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,

            navigateToProfileScreen = navigateToProfileScreen,
        )
    }
}