package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder

interface MarketplaceOrderDaoService {
    suspend fun insertOrUpdateEntity(entity: MarketplaceOrder): MarketplaceOrder

    suspend fun insertEntity(entity: MarketplaceOrder): Long

    suspend fun insertEntities(Entities: List<MarketplaceOrder>): LongArray

    suspend fun getEntityById(id: UniqueID?): MarketplaceOrder?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,

        userId: UniqueID?,
        isPaid: Boolean?,
        isCanceled: Boolean?,
        orderedProducts: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<MarketplaceOrder>): Int

    suspend fun searchEntities(): List<MarketplaceOrder>

    suspend fun getAllEntities(): List<MarketplaceOrder>

    suspend fun getNumEntities(): Int
}