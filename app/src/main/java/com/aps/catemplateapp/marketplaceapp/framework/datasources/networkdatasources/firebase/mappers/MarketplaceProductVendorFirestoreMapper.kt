package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceProductVendorFirestoreEntity
import javax.inject.Inject

class MarketplaceProductVendorFirestoreMapper
@Inject
constructor() : EntityMapper<MarketplaceProductVendorFirestoreEntity, MarketplaceProductVendor> {
    override fun mapFromEntity(entity: MarketplaceProductVendorFirestoreEntity): MarketplaceProductVendor {
        val marketplaceProductVendor = MarketplaceProductVendor(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.name,
            entity.description,
        )
        if(entity.geoLocation != null){
            marketplaceProductVendor.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return marketplaceProductVendor
    }

    override fun mapToEntity(domainModel: MarketplaceProductVendor): MarketplaceProductVendorFirestoreEntity {
        return MarketplaceProductVendorFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceProductVendorFirestoreEntity>) : List<MarketplaceProductVendor> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceProductVendor>): List<MarketplaceProductVendorFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}