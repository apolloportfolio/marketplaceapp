package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID

@Entity(tableName = "marketplaceorder")
data class MarketplaceOrderCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "userId")
    var userId : UniqueID?,

    @ColumnInfo(name = "isPaid")
    var isPaid : Boolean?,

    @ColumnInfo(name = "isCanceled")
    var isCanceled : Boolean?,

    @ColumnInfo(name = "orderedProducts")
    var orderedProducts : String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "name")
    var name: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "ownerID")
    var ownerID: UserUniqueID?,

    ) {
}