package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductCategoryCacheEntity
import javax.inject.Inject

class MarketplaceProductCategoryCacheMapper
@Inject
constructor() : EntityMapper<MarketplaceProductCategoryCacheEntity, MarketplaceProductCategory> {
    override fun mapFromEntity(entity: MarketplaceProductCategoryCacheEntity): MarketplaceProductCategory {
        return MarketplaceProductCategory(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.parentId,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: MarketplaceProductCategory): MarketplaceProductCategoryCacheEntity {
        return MarketplaceProductCategoryCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.parentId,

            domainModel.picture1URI,

            domainModel.name,
            domainModel.description,

            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceProductCategoryCacheEntity>) : List<MarketplaceProductCategory> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceProductCategory>): List<MarketplaceProductCategoryCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}