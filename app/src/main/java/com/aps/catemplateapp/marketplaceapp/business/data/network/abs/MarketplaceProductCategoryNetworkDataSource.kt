package com.aps.catemplateapp.marketplaceapp.business.data.network.abs

import com.aps.catemplateapp.common.business.data.network.StandardNetworkDataSource
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory

interface MarketplaceProductCategoryNetworkDataSource: StandardNetworkDataSource<MarketplaceProductCategory> {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: MarketplaceProductCategory)

    override suspend fun insertDeletedEntities(Entities: List<MarketplaceProductCategory>)

    override suspend fun deleteDeletedEntity(entity: MarketplaceProductCategory)

    override suspend fun getDeletedEntities(): List<MarketplaceProductCategory>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory?

    override suspend fun getAllEntities(): List<MarketplaceProductCategory>

    override suspend fun insertOrUpdateEntities(Entities: List<MarketplaceProductCategory>): List<MarketplaceProductCategory>?

    override suspend fun getEntityById(id: UniqueID): MarketplaceProductCategory?

    suspend fun getUsersMarketplaceProductCategory(userId: UserUniqueID): List<MarketplaceProductCategory>?
}