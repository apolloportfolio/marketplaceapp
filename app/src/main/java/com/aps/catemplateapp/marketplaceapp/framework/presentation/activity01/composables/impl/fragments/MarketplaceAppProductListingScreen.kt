package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Numbers
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.clickWithColoredRipple
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ImageFromFirebase
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType02
import com.aps.catemplateapp.common.framework.presentation.views.RowWithLabelAndValueType01
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductVendorFactory
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsMarketplaceApp
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.ColorTransparent
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.google.firebase.storage.StorageReference

// HomeScreenCompDetailsScreen2
@Composable
fun MarketplaceAppMarketplaceProductListingScreenContent(
    marketplaceProducts: List<MarketplaceProduct>?,
    vendors: List<MarketplaceProductVendor>?,
    onVendorClick: (MarketplaceProductVendor) -> Unit,
    addMarketplaceProductToCart: (MarketplaceProduct) -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        modifier = Modifier
                .fillMaxSize(),
        backgroundDrawableId = R.drawable.marketplace_app_background,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        var selectedImageRef: StorageReference? by remember { mutableStateOf(null) }
        var selectedImageDrawable: Int by remember {
            mutableStateOf(R.drawable.marketplace_app_example_product_0)
        }
        val marketplaceProduct by remember { mutableStateOf(marketplaceProducts?.get(0)) }
        val productThumbnailHeight = remember { 300.dp }

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding)
        ) {
            item {
                ItemPictures(
                    marketplaceProduct = marketplaceProduct,
                    modifier = Modifier
                        .wrapContentWidth()
                        .wrapContentHeight(),
                    selectedImageDrawable = selectedImageDrawable,
                    selectedImageRef = selectedImageRef,
                    setSelectedImageRef = { selectedImageRef = it },
                    setSelectedImageDrawable = { selectedImageDrawable = it },
                    composableHeight = productThumbnailHeight,
                    isPreview = isPreview,
                )
            }
            item {
                if(!marketplaceProducts.isNullOrEmpty()) {
                    Spacer(modifier = Modifier.height(Dimens.spacingMedium))

                    MarketplaceProductDetailsSection(
                        marketplaceProduct = marketplaceProduct,
                        modifier = Modifier
                            .fillMaxWidth(),
                    )

                } else {
                    TipContentIsUnavailable(
                        tip = stringResource(R.string.content_unavailable),
                    )
                }
            }
            item {
                if(!marketplaceProducts.isNullOrEmpty() && vendors != null) {
                    ListingsListSection(
                        marketplaceProducts,
                        vendors,
                        onVendorClick,
                        addMarketplaceProductToCart,
                        modifier = Modifier
                            .fillMaxWidth(),
                        isPreview = isPreview,
                    )
                }
            }
        }
    }
}

@Composable
fun ItemPictures(
    marketplaceProduct: MarketplaceProduct?,
    selectedImageDrawable: Int,
    selectedImageRef: StorageReference?,
    setSelectedImageRef: (StorageReference?) -> Unit,
    setSelectedImageDrawable: (Int) -> Unit,
    composableHeight: Dp,
    modifier: Modifier,

    isPreview: Boolean,
) {
    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
    ) {
        if(marketplaceProduct != null) {
            if(isPreview) {
                Row(
                    modifier = modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.Top,
                    horizontalArrangement = Arrangement.Center,
                ) {

                    Image(
                        painter = fallbackPainterResource(id = selectedImageDrawable),
                        contentDescription = null,
                        modifier = Modifier
                            .height(composableHeight)
                            .width(composableHeight)
                            .background(ColorTransparent)
                            .padding(Dimens.paddingMedium),
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = null,
                    )

                    ImagesLazyColumn(
                        modifier = Modifier,
                        imageDrawableIds = listOf(
                            R.drawable.marketplace_app_example_product_0,
                            R.drawable.marketplace_app_example_product_0_0,
                            R.drawable.marketplace_app_example_product_0_1,
                            R.drawable.marketplace_app_example_product_0_2,
                        ),
                        onClick = setSelectedImageDrawable,
                        columnHeight = composableHeight,
                    )
                }
            } else if (selectedImageRef != null) {
                Row(
                    modifier = modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.Top,
                ) {

                    ImageFromFirebase(
                        selectedImageRef,
                        modifier = modifier
                            .height(200.dp)
                            .background(ColorTransparent)
                            .padding(Dimens.paddingMedium)
                            .fillMaxWidth(),
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )

                    val imageReferences = remember {
                        listOfNotNull(
                            marketplaceProduct.picture2FirebaseImageRef,
                            marketplaceProduct.picture3FirebaseImageRef,
                            marketplaceProduct.picture4FirebaseImageRef,
                            marketplaceProduct.picture5FirebaseImageRef,
                            marketplaceProduct.picture6FirebaseImageRef,
                            marketplaceProduct.picture7FirebaseImageRef,
                            marketplaceProduct.picture8FirebaseImageRef,
                            marketplaceProduct.picture9FirebaseImageRef,
                            marketplaceProduct.picture10FirebaseImageRef
                        )
                    }
                    if(imageReferences.isNotEmpty()) {
                        FirebaseImagesLazyColumn(
                            modifier = Modifier,
                            imageReferences = imageReferences,
                            onClick = setSelectedImageRef,
                            columnHeight = composableHeight,
                        )
                    }
                }
            }
        } else {
            TipContentIsUnavailable(
                tip = stringResource(id = R.string.content_unavailable),
                modifier = modifier,
            )
        }
    }
}

@Composable
fun MarketplaceProductDetailsSection(
    marketplaceProduct: MarketplaceProduct?,
    modifier: Modifier,
) {
    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        if(marketplaceProduct == null) {
            TipContentIsUnavailable(
                stringResource(id = R.string.content_unavailable),
            )
        } else {
            val cornerRadius = 8.dp
            val formattedPrice: String =
                String.format("%.2f", marketplaceProduct.price) + marketplaceProduct.currency
            val productDetails = listOf(
                Triple(
                    Icons.Default.Numbers,
                    stringResource(id = R.string.product_name),
                    marketplaceProduct.productName ?: ""
                ),
                Triple(
                    Icons.Default.Numbers,
                    stringResource(id = R.string.price),
                    formattedPrice
                ),
                Triple(
                    Icons.Default.Numbers,
                    stringResource(id = R.string.brand),
                    marketplaceProduct.brand ?: ""
                ),
                Triple(
                    Icons.Default.Numbers,
                    stringResource(id = R.string.description),
                    marketplaceProduct.description ?: ""
                ),
            )

            Column(
                modifier = Modifier
                    .padding(Dimens.columnPadding),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.spacedBy(Dimens.spacingSmall)
            ) {
                for(detail in productDetails) {
                    RowWithLabelAndValueType01(
                        icon = detail.first,
                        label = detail.second,
                        value = detail.third,
                        tint = MaterialTheme.colors.onBackground,
                        borderWidth = 2.dp,
                        cornerRadius = cornerRadius,
                        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                            colors = listOf(
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.primaryVariant,
                                MaterialTheme.colors.primary,
                            ),
                            brush = null,
                            shape = RoundedCornerShape(cornerRadius),
                            alpha = 0.7f,
                        ),
                    )
                }
            }
        }
    }
}

@Composable
fun FirebaseImagesLazyColumn(
    imageReferences: List<StorageReference>,
    onClick: (StorageReference?) -> Unit,
    columnHeight: Dp,
    itemPaddingValues: PaddingValues = PaddingValues(4.dp, 4.dp, 4.dp, 4.dp),
    modifier: Modifier,
) {
    val calculatedItemSize = maxOf((columnHeight / imageReferences.size), 32.dp)

    LazyColumn(
        modifier = modifier
            .height(columnHeight)
            .wrapContentWidth(),
        verticalArrangement = Arrangement.Center,
    ) {
        itemsIndexed(imageReferences) { index, item ->
            Box {
                ImageFromFirebase(
                    firebaseImageRef = item,
                    modifier = Modifier
                        .align(Alignment.Center)
                        .height(calculatedItemSize)
                        .width(calculatedItemSize)
                        .background(ColorTransparent)
                        .clickWithColoredRipple(
                            color = MaterialTheme.colors.primary,
                            onClick = { onClick(item) }
                        ),
                    contentDescription = index.toString(),
                    contentScale = ContentScale.Crop
                )
            }
        }
    }
}

@Composable
fun ImagesLazyColumn(
    imageDrawableIds: List<Int>,
    onClick: (Int) -> Unit,
    columnHeight: Dp,
    itemPaddingValues: PaddingValues = PaddingValues(4.dp, 4.dp, 4.dp, 4.dp),
    modifier: Modifier,
) {
    val calculatedItemSize = maxOf((columnHeight / imageDrawableIds.size), 32.dp)

    LazyColumn(
        modifier = modifier
            .height(columnHeight)
            .wrapContentWidth(),
        verticalArrangement = Arrangement.Center,
    ) {
        itemsIndexed(imageDrawableIds) { index, item ->
            Box {
                Image(
                    painter = fallbackPainterResource(id = item),
                    contentDescription = null,
                    modifier = Modifier
                        .align(Alignment.Center)
                        .height(calculatedItemSize)
                        .width(calculatedItemSize)
                        .background(ColorTransparent)
                        .padding(itemPaddingValues)
                        .clickWithColoredRipple(
                            color = MaterialTheme.colors.primary,
                            onClick = { onClick(item) }
                        ),
                    alignment = Alignment.Center,
                    contentScale = ContentScale.Fit,
                    alpha = DefaultAlpha,
                    colorFilter = null,
                )
            }
        }
    }
}

@Composable
private fun ListingsListSection(
    marketplaceProducts: List<MarketplaceProduct>,
    vendors: List<MarketplaceProductVendor>,
    onVendorClick: (MarketplaceProductVendor) -> Unit,
    addMarketplaceProductToCart: (MarketplaceProduct) -> Unit,
    modifier: Modifier,

    isPreview: Boolean = false,
) {
    val marketplaceProductsWithVendors = remember {
        mutableListOf<Pair<MarketplaceProduct, MarketplaceProductVendor>>()
    }
    for(marketplaceProduct in marketplaceProducts) {
        for(vendor in vendors) {
//            if(vendor.id == marketplaceProduct.productVendorID && vendor.id != null) {
//                marketplaceProductsWithVendors.add(Pair(marketplaceProduct, vendor))
//            }
            val vendorId = vendor.id
            val productVendorID = marketplaceProduct.productVendorID
            if(vendorId != null && productVendorID != null ) {
                if(vendorId.equalByValueTo(productVendorID)) {
                    marketplaceProductsWithVendors.add(Pair(marketplaceProduct, vendor))
                }
            }

        }
    }

    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .padding(Dimens.columnPadding)
        ) {
            TitleRowType01(
                titleString = stringResource(id = R.string.product_listings),
                textAlign = TextAlign.Start,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundTitleType01(
                    colors = listOf(
                        MaterialTheme.colors.surface,
                        MaterialTheme.colors.primaryVariant,
                        MaterialTheme.colors.primary,
                    ),
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                titleFontSize = Dimens.titleFontSize,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .testTag("ProductListings"),
            )

            LazyColumn(
                modifier = Modifier
                    .height(200.dp)
            ) {
                itemsIndexed(marketplaceProductsWithVendors) { index, productAndVendor ->
                    val product = productAndVendor.first
                    val vendor = productAndVendor.second
                    val priceAndCurrency = if(product.price != null && product.currency != null)
                        "${product.price} ${product.currency}"
                    else ""
                    ListItemType02(
                        index = index,
                        itemTitleString = vendor.name,
                        itemDescription = priceAndCurrency,
                        onListItemClick = { onVendorClick(vendor) },
                        onItemsButtonClick = { addMarketplaceProductToCart(product) },
                        getItemsRating = { null },
                        itemRef = if (isPreview) {
                            null
                        } else {
                            vendor.picture1FirebaseImageRef
                        },
                        itemDrawableId = if(isPreview) {
                            R.drawable.marketplace_app_example_product_0
                        } else { null },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                            colors = listOf(
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.primary,
                            ),
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        imageTint = null,
                    )
                }
            }
        }
    }
}

// Preview ========================================================================================
@Preview
@Composable
private fun MarketplaceAppMarketplaceProductListingScreenPreview() {
    HomeScreenTheme {
        val marketplaceProduct = MarketplaceProductFactory.createPreviewMarketplaceProductsList()[0]
        val numberOfListings = 10
        val marketplaceProductListings = List(numberOfListings) {
            marketplaceProduct
        }
        val adjustmentValues = listOf(-10, -5, 0, 5, 10)
        var adjustmentIndex = 0
        marketplaceProductListings.forEach { product ->
            val originalPrice: Double = product.price ?: 100.0
            val adjustment = adjustmentValues[adjustmentIndex]
            adjustmentIndex = (adjustmentIndex + 1) % adjustmentValues.size
            val newPrice = originalPrice + adjustment
            product.apply { price = newPrice }
        }
        MarketplaceAppMarketplaceProductListingScreenContent(
            marketplaceProducts = marketplaceProductListings,
            vendors = MarketplaceProductVendorFactory.createPreviewEntitiesList(),
            onVendorClick = {},
            addMarketplaceProductToCart = {},

            isPreview = true,
        )
    }
}
