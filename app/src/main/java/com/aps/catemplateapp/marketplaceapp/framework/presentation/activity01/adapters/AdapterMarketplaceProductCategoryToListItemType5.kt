package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.core.util.ProjectConstants
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

private const val TAG = "AdapterMarketplaceProductCategoryToListItemType5"
private const val LOG_ME = true

class AdapterMarketplaceProductCategoryToListItemType5(
    private var objectsList : ArrayList<MarketplaceProductCategory>?,
    private val onItemClickListener: OnItemClickListener?,
    private val onButtonClickListener: OnItemClickListener?,
    private val onThumbnailClickListener: OnItemClickListener?,
    )
    : RecyclerView.Adapter<AdapterMarketplaceProductCategoryToListItemType5.ListItemType5ViewHolder>() {

    lateinit var context : Context

    fun getData() = objectsList
    fun setData(data : ArrayList<MarketplaceProductCategory>?) {
        objectsList = data
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemType5ViewHolder {
        context = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_type5,
            parent,
            false
        )
        return ListItemType5ViewHolder(itemView)
    }

    @SuppressLint("LongLogTag")
    override fun onBindViewHolder(holder: ListItemType5ViewHolder, position: Int) {
        val methodName: String = "onBindViewHolder"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(objectsList != null) {
                val currentItem = objectsList!![position]


                setImage(currentItem, holder)
            } else {
                Log.w(TAG, "onBindViewHolder: objectsList == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun getItemCount(): Int {
        return objectsList?.size ?: 0
    }

    inner class ListItemType5ViewHolder(itemView : View)
        : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val imgThumbnail : ImageView = itemView.findViewById(R.id.item_thumbnail)
        val lblTitle : TextView = itemView.findViewById(R.id.item_title)
        val number : TextView = itemView.findViewById(R.id.lblInfo)
        val button : ImageButton = itemView.findViewById(R.id.button)
        init {
            itemView.setOnClickListener(this)
            if(onThumbnailClickListener != null)imgThumbnail.setOnClickListener(this)
            if(onButtonClickListener != null)button.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                when(v) {
                    imgThumbnail -> {
                        onThumbnailClickListener?.onItemClick(position)
                        return
                    }
                    button -> {
                        onButtonClickListener?.onItemClick(position)
                        return
                    }
                }
                onItemClickListener?.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    private fun setImage(
        marketplaceProductCategory : MarketplaceProductCategory,
        holder : ListItemType5ViewHolder
    ) {
        val methodName: String = "setImage"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(marketplaceProductCategory.picture1URI == null) {
                if(LOG_ME) ALog.w(
                    TAG, ".$methodName(): " +
                            "rentalOffer.picture1URI == null"
                )
                return
            }
            if(marketplaceProductCategory.id == null) {
                if(LOG_ME) ALog.w(
                    TAG, ".$methodName(): " +
                            "rentalOffer.id == null"
                )
                return
            }

            val glideOptions: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)

            val storageReference = Firebase.storage
            val imageRef = storageReference.reference
                .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                .child(ProjectConstants.FIRESTORE_ENTITY_2_IMAGES_SUB_COLLECTION)
                .child(marketplaceProductCategory.id!!.firestoreDocumentID)
                .child(marketplaceProductCategory.picture1URI!!)

            if (LOG_ME) ALog.d(TAG, ".$methodName(): $imageRef")

            Glide.with(holder.imgThumbnail)
                .load(imageRef)
                .apply(glideOptions)
                .into(holder.imgThumbnail)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}