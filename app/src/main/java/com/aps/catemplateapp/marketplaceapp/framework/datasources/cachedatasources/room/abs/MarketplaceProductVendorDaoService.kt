package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor

interface MarketplaceProductVendorDaoService {
    suspend fun insertOrUpdateEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor

    suspend fun insertEntity(entity: MarketplaceProductVendor): Long

    suspend fun insertEntities(Entities: List<MarketplaceProductVendor>): LongArray

    suspend fun getEntityById(id: UniqueID?): MarketplaceProductVendor?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<MarketplaceProductVendor>): Int

    suspend fun searchEntities(): List<MarketplaceProductVendor>

    suspend fun getAllEntities(): List<MarketplaceProductVendor>

    suspend fun getNumEntities(): Int
}