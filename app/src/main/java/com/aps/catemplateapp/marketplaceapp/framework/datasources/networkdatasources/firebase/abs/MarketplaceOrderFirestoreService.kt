package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers.MarketplaceOrderFirestoreMapper
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceOrderFirestoreEntity

interface MarketplaceOrderFirestoreService: BasicFirestoreService<
        MarketplaceOrder,
        MarketplaceOrderFirestoreEntity,
        MarketplaceOrderFirestoreMapper
        > {
    suspend fun getUsersMarketplaceOrders(userId: UserUniqueID): List<MarketplaceOrder>?

}