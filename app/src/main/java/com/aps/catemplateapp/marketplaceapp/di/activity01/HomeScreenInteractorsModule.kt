package com.aps.catemplateapp.marketplaceapp.di.activity01

import com.aps.catemplateapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.catemplateapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceOrderCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCategoryCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceOrderNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductCategoryNetworkDataSource
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceOrderFactory
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductCategoryFactory
import com.aps.catemplateapp.core.util.SecureKeyStorage
import com.aps.catemplateapp.marketplaceapp.business.interactors.abs.*
import com.aps.catemplateapp.marketplaceapp.business.interactors.impl.*
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenInteractorsModule {

    @Provides
    fun provideGetUsersRating(
        cacheDataSource: UserCacheDataSource,
        networkDataSource: UserNetworkDataSource,
    ): GetUsersRating {
        return GetUsersRatingImpl(cacheDataSource, networkDataSource)
    }

    @Provides
    fun InitiateGooglePayPaymentProcess(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): InitiateGooglePayPaymentProcess {
        return InitiateGooglePayPaymentProcessImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetMerchantName(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetMerchantName {
        return GetMerchantNameImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideGetGatewayNameAndMerchantID(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
    ): GetGatewayNameAndMerchantID {
        return GetGatewayNameAndMerchantIDImpl(secureKeyStorage, dateUtil)
    }

    @Provides
    fun provideDownloadExchangeRates(): DownloadExchangeRates {
        return DownloadExchangeRatesImpl()
    }

    @Provides
    fun provideCheckGooglePayAvailability(): CheckGooglePayAvailability {
        return CheckGooglePayAvailabilityImpl()
    }

    @Provides
    fun provideLogoutUser(
        userCacheDataSource: UserCacheDataSource,
        userNetworkDataSource: UserNetworkDataSource,
    ): LogoutUser {
        return LogoutUserImpl(
            userCacheDataSource,
            userNetworkDataSource,
        )
    }

    @Provides
    fun provideGetEntities1AroundUser(
        cacheDataSource: MarketplaceProductCacheDataSource,
        networkDataSource: MarketplaceProductNetworkDataSource,
        entityFactory: MarketplaceProductFactory
    ): GetMarketplaceProductsAroundUser {
        return GetMarketplaceProductsAroundUserImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }


    @Provides
    fun provideGetEntities2(
        cacheDataSource: MarketplaceOrderCacheDataSource,
        networkDataSource: MarketplaceOrderNetworkDataSource,
        entityFactory: MarketplaceOrderFactory
    ): GetMarketplaceOrders {
        return GetMarketplaceOrdersImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideGetEntities3(
        cacheDataSource: MarketplaceProductCategoryCacheDataSource,
        networkDataSource: MarketplaceProductCategoryNetworkDataSource,
        entityFactory: MarketplaceProductCategoryFactory
    ): GetMarketplaceProductCategorys {
        return GetMarketplaceProductCategorysImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
        )
    }

    @Provides
    fun provideSearchEntities1(
        cacheDataSource: MarketplaceProductCacheDataSource,
        networkDataSource: MarketplaceProductNetworkDataSource,
        entityFactory: MarketplaceProductFactory
    ): SearchMarketplaceProducts {
        return SearchMarketplaceProductsImpl(
            cacheDataSource,
            networkDataSource,
        )
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>>()
    }
}