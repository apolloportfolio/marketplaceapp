package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers.MarketplaceProductCategoryFirestoreMapper
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceProductCategoryFirestoreEntity

interface MarketplaceProductCategoryFirestoreService: BasicFirestoreService<
        MarketplaceProductCategory,
        MarketplaceProductCategoryFirestoreEntity,
        MarketplaceProductCategoryFirestoreMapper
        > {
    suspend fun getUsersMarketplaceProductCategorys(userId: UserUniqueID): List<MarketplaceProductCategory>?

}