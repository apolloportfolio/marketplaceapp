package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceOrderFirestoreEntity
import javax.inject.Inject

class MarketplaceOrderFirestoreMapper
@Inject
constructor() : EntityMapper<MarketplaceOrderFirestoreEntity, MarketplaceOrder> {
    override fun mapFromEntity(entity: MarketplaceOrderFirestoreEntity): MarketplaceOrder {
        return MarketplaceOrder(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.userId,
            entity.isPaid,
            entity.isCanceled,
            entity.orderedProducts,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: MarketplaceOrder): MarketplaceOrderFirestoreEntity {
        return MarketplaceOrderFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.userId,
            domainModel.isPaid,
            domainModel.isCanceled,
            domainModel.orderedProducts,

            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceOrderFirestoreEntity>) : List<MarketplaceOrder> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceOrder>): List<MarketplaceOrderFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}