package com.aps.catemplateapp.marketplaceapp.business.interactors.abs

import android.location.Location
import com.aps.catemplateapp.common.business.domain.state.DataState
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface GetMarketplaceProductsAroundUser {
    fun getMarketplaceProductsAroundUser(
        location : Location?,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<MarketplaceProduct>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit = {},
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}