package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceProductCategoryFirestoreEntity
import javax.inject.Inject

class MarketplaceProductCategoryFirestoreMapper
@Inject
constructor() : EntityMapper<MarketplaceProductCategoryFirestoreEntity, MarketplaceProductCategory> {
    override fun mapFromEntity(entity: MarketplaceProductCategoryFirestoreEntity): MarketplaceProductCategory {
        return MarketplaceProductCategory(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.parentId,

            entity.picture1URI,

            entity.name,
            entity.description,

            entity.ownerID,
        )
    }

    override fun mapToEntity(domainModel: MarketplaceProductCategory): MarketplaceProductCategoryFirestoreEntity {
        return MarketplaceProductCategoryFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.parentId,
            domainModel.picture1URI,
            domainModel.name,
            domainModel.description,
            domainModel.ownerID,
        )
    }

    override fun mapFromEntityList(entities : List<MarketplaceProductCategoryFirestoreEntity>) : List<MarketplaceProductCategory> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<MarketplaceProductCategory>): List<MarketplaceProductCategoryFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}