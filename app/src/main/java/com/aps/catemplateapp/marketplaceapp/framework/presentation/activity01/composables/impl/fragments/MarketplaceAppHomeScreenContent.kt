package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.TrendingUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.domain.state.StateEventTracker
import com.aps.catemplateapp.common.framework.presentation.layouts.LazyGridWithPullToRefresh
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType03
import com.aps.catemplateapp.common.framework.presentation.views.ProfileStatusBar
import com.aps.catemplateapp.common.framework.presentation.views.SearchBarWithClearButton
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.DeviceLocation
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsMarketplaceApp
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme


private const val TAG = "MarketplaceAppHomeScreenContent"
private const val LOG_ME = true
const val MarketplaceAppHomeScreenContentTestTag = TAG

// HomeScreenComposableFragment1
// Displays personalized product recommendations, trending products,
// and featured collections, providing a quick and easy way for users
// to discover new products. It contains a grid with products.
// Each item in the grid has product's picture and name below it.

@Composable
internal fun MarketplaceAppHomeScreenContent(
    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialSearchQuery: String = "",
    trendingMarketplaceProducts: List<MarketplaceProduct>?,
    recommendedMarketplaceProducts: List<MarketplaceProduct>?,
    onSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (MarketplaceProduct) -> Unit,
    launchInitStateEvent: () -> Unit,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    BoxWithBackground(
        backgroundDrawableId = R.drawable.marketplace_app_background,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding),
            verticalArrangement = Arrangement.Center,
        ) {
            var searchQuery by remember { mutableStateOf(initialSearchQuery) }
            var searchQueryUpdated by remember { mutableStateOf(false) }

            SearchBarWithClearButton(
                searchQuery = searchQuery,
                onSearchQueryChange = { query -> searchQuery = query },
                onSearchBackClick = {
                    searchQuery = ""
                    searchQueryUpdated = true
                },
                onSearchClick = {
                    searchQueryUpdated = true
                },
                composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
                    colors = listOf(
                        MaterialTheme.colors.surface,
                        MaterialTheme.colors.primaryVariant,
                        MaterialTheme.colors.primary,
                    ),
                    brush = null,
                    shape = null,
                    alpha = 0.6f,
                ),
            )

            if(LOG_ME) ALog.d(
                TAG, "(): " +
                    "showProfileStatusBar = $showProfileStatusBar")
            if(showProfileStatusBar) {
                if(LOG_ME) ALog.d(
                    TAG, "(): " +
                        "Showing profile completion bar")
                ProfileStatusBar(
                    finishVerification,
                    composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
                        colors = listOf(
                            MaterialTheme.colors.surface,
                            MaterialTheme.colors.primaryVariant,
                            MaterialTheme.colors.primary,
                        ),
                        brush = null,
                        shape = null,
                        alpha = 0.6f,
                    ),
                    titleTint = MaterialTheme.colors.secondary,
                    subtitleTint = MaterialTheme.colors.error,)
            }

            val noEntitiesToShow =
                trendingMarketplaceProducts.isNullOrEmpty() &&
                        recommendedMarketplaceProducts.isNullOrEmpty()
            if(!noEntitiesToShow) {

                if(!trendingMarketplaceProducts.isNullOrEmpty()) {
                    FeaturedSection(
                        title = stringResource(id = R.string.trending_now),
                        icon = Icons.Default.TrendingUp,
                        entities = trendingMarketplaceProducts,
                        stateEventTracker = stateEventTracker,
                        deviceLocation = deviceLocation,
                        launchInitStateEvent = launchInitStateEvent,
                        onListItemClick = onListItemClick,
                        isPreview = isPreview,
                    )
                }

                if(!recommendedMarketplaceProducts.isNullOrEmpty()) {
                    FeaturedSection(
                        title = stringResource(id = R.string.recommended),
                        icon = Icons.Default.Star,
                        entities = recommendedMarketplaceProducts,
                        stateEventTracker = stateEventTracker,
                        deviceLocation = deviceLocation,
                        launchInitStateEvent = launchInitStateEvent,
                        onListItemClick = onListItemClick,
                        isPreview = isPreview,
                    )
                }

            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment1_list_is_empty_tip))
            }

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchQueryUpdated) {
                if(firstComposition) {
                    if(LOG_ME) ALog.d(
                        TAG, "LaunchedEffect(searchFiltersUpdated): " +
                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchQueryUpdated) {
                        if(LOG_ME) ALog.d(TAG, "(): LaunchedEffect: searchQueryUpdated")
                        onSearchQueryUpdate(searchQuery)
                    }
                }
                searchQueryUpdated = false
            }

            if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
        }
    }

}

@Composable
private fun FeaturedSection(
    title: String,
    icon: ImageVector,
    entities: List<MarketplaceProduct>,
    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    launchInitStateEvent: () -> Unit,
    onListItemClick: (MarketplaceProduct) -> Unit,
    isPreview: Boolean,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column {
            TitleRowType01(
                titleString = title,
                textAlign = TextAlign.Center,
                composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundTitleType01(
                    colors = listOf(
                        MaterialTheme.colors.surface,
                        MaterialTheme.colors.primaryVariant,
                        MaterialTheme.colors.primary,
                    ),
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                leftPictureDrawableId = null,
                titleFontSize = Dimens.titleFontSize,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .fillMaxWidth()
            )

            LazyGridWithPullToRefresh(
                modifier = Modifier,
                contentPadding = PaddingValues(horizontal = 4.dp, vertical = 4.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                onRefresh = launchInitStateEvent,
                isRefreshing = stateEventTracker.isRefreshing,
                columns = GridCells.Fixed(2),
            ) {
                itemsIndexed(entities) { index, item ->
                    ListItemType03(
                        index = index,
                        itemTitleString = item.productName,
                        itemPriceString = "",
                        itemLikesString = "",
                        itemDistanceString = item.brand ?: "",
                        lblSmallShortAttribute3String = "",
                        itemDescriptionString = item.description,
                        onListItemClick = { onListItemClick(item) },
                        getItemsRating = { null },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            item.picture1FirebaseImageRef
                        },
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("marketplace_app_example_product_", index)
                        } else { null },
                        imageTint = null,
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
                            colors = listOf(
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.primary,
                            ),
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                    )
                }
            }
        }
    }
}

@Preview
@Composable
private fun MarketplaceAppHomeScreenContentPreview() {
    HomeScreenTheme {
        MarketplaceAppHomeScreenContent(
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            deviceLocation = DeviceLocation.PreviewDeviceLocation,
            showProfileStatusBar = false,
            finishVerification = {},
            initialSearchQuery = "",
            trendingMarketplaceProducts = MarketplaceProductFactory.createPreviewMarketplaceProductsList(),
            recommendedMarketplaceProducts = MarketplaceProductFactory.createPreviewMarketplaceProductsList(),
            onSearchQueryUpdate = {},
            onListItemClick = {},
            launchInitStateEvent = {},

            isPreview = true,
        )
    }
}
