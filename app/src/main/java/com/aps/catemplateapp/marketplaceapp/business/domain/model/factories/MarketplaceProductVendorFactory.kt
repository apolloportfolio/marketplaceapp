package com.aps.catemplateapp.marketplaceapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductVendorFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): MarketplaceProductVendor {
        return MarketplaceProductVendor(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            name,
            description,
        )
    }

    fun createEntitiesList(numEntities: Int): List<MarketplaceProductVendor> {
        val list: ArrayList<MarketplaceProductVendor> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): MarketplaceProductVendor {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<MarketplaceProductVendor> {
            return arrayListOf(
                MarketplaceProductVendor(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "Green Garden Supplies",
                    description = "Provider of quality gardening tools and equipment."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "FashionFusion Boutique",
                    description = "Trendy fashion items for all occasions."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "TechSmart Solutions",
                    description = "Innovative technology solutions for businesses and individuals."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "Healthy Bites Café",
                    description = "Delicious and nutritious food options for health-conscious individuals."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "Bookworm Haven",
                    description = "A paradise for book lovers, offering a wide range of titles across genres."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "SportsGear Hub",
                    description = "Top-quality sports equipment and apparel for enthusiasts of all levels."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "HomeStyle Décor",
                    description = "Elegant home décor items to elevate your living spaces."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "Pet Paradise",
                    description = "Everything your beloved pets need for a happy and healthy life."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "Artistic Expressions",
                    description = "Inspiring artworks and creative supplies for artists of all kinds."
                ),
                MarketplaceProductVendor(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    name = "TechTrends Store",
                    description = "Stay ahead of the curve with the latest gadgets and tech innovations."
                )
            )
        }

    }
}