package com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation


import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceOrderCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceOrderDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceOrderCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: MarketplaceOrderDaoService
): MarketplaceOrderCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceOrder): MarketplaceOrder? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: MarketplaceOrder): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceOrder>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,

        userId: UniqueID?,
        isPaid: Boolean?,
        isCanceled: Boolean?,
        orderedProducts: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            name,
            description,

            ownerID,

            userId,
            isPaid,
            isCanceled,
            orderedProducts,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceOrder> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<MarketplaceOrder> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceOrder? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<MarketplaceOrder>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}