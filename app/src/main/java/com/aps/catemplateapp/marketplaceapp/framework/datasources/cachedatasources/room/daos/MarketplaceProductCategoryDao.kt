package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductCategoryCacheEntity
import java.util.*


@Dao
interface MarketplaceProductCategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : MarketplaceProductCategoryCacheEntity) : Long

    @Query("SELECT * FROM marketplaceproductcategory")
    suspend fun get() : List<MarketplaceProductCategoryCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<MarketplaceProductCategoryCacheEntity>): LongArray

    @Query("SELECT * FROM marketplaceproductcategory WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): MarketplaceProductCategoryCacheEntity?

    @Query("DELETE FROM marketplaceproductcategory WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM marketplaceproductcategory")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM marketplaceproductcategory")
    suspend fun getAllEntities(): List<MarketplaceProductCategoryCacheEntity>

    @Query("""
        UPDATE marketplaceproductcategory 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        parentId = :parentId,
        picture1URI = :picture1URI,
        name = :name,
        description = :description,
        ownerId = :ownerID
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,
        parentId: UniqueID?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    @Query("DELETE FROM marketplaceproductcategory WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM marketplaceproductcategory")
    suspend fun searchEntities(): List<MarketplaceProductCategoryCacheEntity>
    
    @Query("SELECT COUNT(*) FROM marketplaceproductcategory")
    suspend fun getNumEntities(): Int
}