package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductVendorCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface MarketplaceProductVendorDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : MarketplaceProductVendorCacheEntity) : Long

    @Query("SELECT * FROM marketplaceproductvendor")
    suspend fun get() : List<MarketplaceProductVendorCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<MarketplaceProductVendorCacheEntity>): LongArray

    @Query("SELECT * FROM marketplaceproductvendor WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): MarketplaceProductVendorCacheEntity?

    @Query("DELETE FROM marketplaceproductvendor WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM marketplaceproductvendor")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM marketplaceproductvendor")
    suspend fun getAllEntities(): List<MarketplaceProductVendorCacheEntity>

    @Query(
        """
        UPDATE marketplaceproductvendor 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        name = :name,
        description = :description
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        name: String?,
        description : String?,
    ): Int

    @Query("DELETE FROM marketplaceproductvendor WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM marketplaceproductvendor")
    suspend fun searchEntities(): List<MarketplaceProductVendorCacheEntity>
    
    @Query("SELECT COUNT(*) FROM marketplaceproductvendor")
    suspend fun getNumEntities(): Int
}