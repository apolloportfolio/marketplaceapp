package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceOrderCacheEntity
import java.util.*


@Dao
interface MarketplaceOrderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : MarketplaceOrderCacheEntity) : Long

    @Query("SELECT * FROM marketplaceorder")
    suspend fun get() : List<MarketplaceOrderCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<MarketplaceOrderCacheEntity>): LongArray

    @Query("SELECT * FROM marketplaceorder WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): MarketplaceOrderCacheEntity?

    @Query("DELETE FROM marketplaceorder WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM marketplaceorder")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM marketplaceorder")
    suspend fun getAllEntities(): List<MarketplaceOrderCacheEntity>

    @Query("""
        UPDATE marketplaceorder 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        name = :name,
        description = :description,
        ownerId = :ownerId,
        userId = :userId,
        isPaid = :isPaid,
        isCanceled = :isCanceled,
        orderedProducts = :orderedProducts
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,
        picture1URI: String?,
        name: String?,
        description: String?,
        ownerId: UserUniqueID?,
        userId: UniqueID?,
        isPaid: Boolean?,
        isCanceled: Boolean?,
        orderedProducts: String?
    ): Int

    @Query("DELETE FROM marketplaceorder WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM marketplaceorder")
    suspend fun searchEntities(): List<MarketplaceOrderCacheEntity>
    
    @Query("SELECT COUNT(*) FROM marketplaceorder")
    suspend fun getNumEntities(): Int
}