package com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.fragments


import android.os.Parcelable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.views.BoxWithBackground
import com.aps.catemplateapp.common.framework.presentation.views.ListItemType05
import com.aps.catemplateapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.catemplateapp.common.framework.presentation.views.TitleRowType01
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsMarketplaceApp
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.google.firebase.storage.StorageReference
import kotlinx.android.parcel.Parcelize

// HomeScreenCompDetailsScreen1

@Composable
fun MarketplaceAppMarketplaceProductDetailsScreenContent(
    marketplaceProduct: MarketplaceProduct?,
    currentlyShownMarketplaceProductsReviews: List<MarketplaceReview>?,
    onMarketplaceReviewClick: (MarketplaceReview) -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        modifier = Modifier
            .fillMaxSize(),
        backgroundDrawableId = R.drawable.marketplace_app_background,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        if(marketplaceProduct == null) {
            TipContentIsUnavailable(tip = stringResource(id = R.string.content_unavailable))
        } else {
            var selectedImageRef: StorageReference? by remember { mutableStateOf(null) }
            var selectedImageDrawable: Int by remember {
                mutableIntStateOf(R.drawable.marketplace_app_example_product_0)
            }
            val productThumbnailHeight = remember { 300.dp }

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(Dimens.columnPadding)
            ) {
                item {
                    ItemPictures(
                        marketplaceProduct = marketplaceProduct,
                        modifier = Modifier
                            .wrapContentWidth(),
                        selectedImageDrawable = selectedImageDrawable,
                        selectedImageRef = selectedImageRef,
                        setSelectedImageRef = { selectedImageRef = it },
                        setSelectedImageDrawable = { selectedImageDrawable = it },
                        composableHeight = productThumbnailHeight,
                        isPreview = isPreview,
                    )
                }
                item {
                    MarketplaceProductDetailsSection(
                        marketplaceProduct = marketplaceProduct,
                        modifier = Modifier
                            .fillMaxWidth(),
                    )
                }
                item {
                    if(!currentlyShownMarketplaceProductsReviews.isNullOrEmpty()) {
                        ReviewsListSection(
                            currentlyShownMarketplaceProductsReviews,
                            onMarketplaceReviewClick,
                            modifier = Modifier
                                .fillMaxWidth(),
                            isPreview = isPreview,
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun ReviewsListSection(
    marketplaceReviews: List<MarketplaceReview>,
    onMarketplaceReviewClick: (MarketplaceReview) -> Unit,
    modifier: Modifier,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .padding(Dimens.columnPadding)
        ) {
            TitleRowType01(
                titleString = stringResource(id = R.string.user_reviews),
                textAlign = TextAlign.Start,
                backgroundDrawableId = R.drawable.marketplace_app_background,
                composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundTitleType01(
                    colors = listOf(
                        MaterialTheme.colors.surface,
                        MaterialTheme.colors.primaryVariant,
                        MaterialTheme.colors.primary,
                    ),
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                titleFontSize = Dimens.titleFontSize,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
            )

            LazyColumn(
                modifier = Modifier
                    .height(200.dp)
            ) {
                itemsIndexed(marketplaceReviews) { index, review ->
                    ListItemType05(
                        index = index,
                        titleString = review.title,
                        descriptionString = review.text,
                        onListItemClick = { onMarketplaceReviewClick(review) },
                        getItemsRating = { review.rating },
                        itemRef = null,
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview(prefix = "example_user_profile_picture_", suffix = index)
                        } else { null },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsMarketplaceApp.backgroundListItemType01(
                            colors = listOf(
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.surface,
                                MaterialTheme.colors.primary,
                            ),
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        imageTint = null,
                    )
                }
            }
        }
    }
}

@Parcelize
data class MarketplaceReview(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,
    val title: String?,
    val text: String?,
    val userId: UniqueID?,
    val productId: UniqueID?,
    val rating: Float?
) : Parcelable {
    companion object {
        fun createExampleReviews(): List<MarketplaceReview> {
            val reviews = mutableListOf<MarketplaceReview>()
            val userIds = List(10) { UniqueID("user${it + 1}") }
            val productIds = List(10) { UniqueID("product${it + 1}") }

            // Predefined titles and texts
            val titles = listOf(
                "Excellent Product",
                "Good Product",
                "Average Product",
                "Below Average Product",
                "Poor Product",
                "Great Purchase",
                "Not Worth It",
                "Highly Recommended",
                "Mixed Feelings",
                "Disappointing Experience"
            )

            val texts = listOf(
                "This product exceeded my expectations. Highly recommended!",
                "A good product overall. I'm satisfied with my purchase.",
                "This product is average. Nothing exceptional, but it gets the job done.",
                "Below my expectations. There are better alternatives available.",
                "I'm very disappointed with this product. Poor quality and performance.",
                "An excellent purchase! I'm extremely happy with this product.",
                "Not worth the price. I expected better quality for the money.",
                "I highly recommend this product to anyone looking for quality and value.",
                "Mixed feelings about this product. Some aspects are good, others not so much.",
                "This product was a huge letdown. I do not recommend it."
            )

            repeat(10) { index ->
                val title = titles.getOrElse(index) { "No Title Available" }
                val text = texts.getOrElse(index) { "No Text Available" }

                val review = MarketplaceReview(
                    id = UniqueID("review${index + 1}"),
                    created_at = "2024-02-13",
                    updated_at = "2024-02-13",
                    title = title,
                    text = text,
                    userId = userIds[index],
                    productId = productIds[index],
                    rating = (5 - (index % 5)).toFloat() // ratings will be 5, 4, 3, 2, or 1
                )
                reviews.add(review)
            }

            return reviews
        }

    }
}

// Preview =========================================================================================
@Preview
@Composable
private fun MarketplaceAppMarketplaceProductDetailsScreenContentPreview() {
    HomeScreenTheme {
        MarketplaceAppMarketplaceProductDetailsScreenContent(
            marketplaceProduct = MarketplaceProductFactory.createPreviewMarketplaceProductsList()[0],
            currentlyShownMarketplaceProductsReviews = MarketplaceReview.createExampleReviews(),
            onMarketplaceReviewClick = {},

            isPreview = true,
        )
    }
}
