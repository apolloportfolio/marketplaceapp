package com.aps.catemplateapp.marketplaceapp.di

import com.aps.catemplateapp.core.business.data.cache.abstraction.*
import com.aps.catemplateapp.core.business.data.cache.implementation.*
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceOrderCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCategoryCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductVendorCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation.MarketplaceProductCacheDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation.MarketplaceOrderCacheDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation.MarketplaceProductCategoryCacheDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation.MarketplaceProductVendorCacheDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindMarketplaceProductCacheDataSource(implementation: MarketplaceProductCacheDataSourceImpl): MarketplaceProductCacheDataSource

    @Binds
    abstract fun bindMarketplaceOrderCacheDataSource(implementation: MarketplaceOrderCacheDataSourceImpl): MarketplaceOrderCacheDataSource

    @Binds
    abstract fun bindMarketplaceProductCategoryCacheDataSource(implementation: MarketplaceProductCategoryCacheDataSourceImpl): MarketplaceProductCategoryCacheDataSource

    @Binds
    abstract fun bindMarketplaceProductVendorCacheDataSource(implementation: MarketplaceProductVendorCacheDataSourceImpl): MarketplaceProductVendorCacheDataSource
}