package com.aps.catemplateapp.marketplaceapp.business.interactors.impl

import android.location.Location
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.business.data.network.ApiResponseHandler
import com.aps.catemplateapp.common.business.data.network.NetworkConstants
import com.aps.catemplateapp.common.business.data.util.safeApiCall
import com.aps.catemplateapp.common.business.domain.state.*
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.marketplaceapp.business.interactors.abs.GetMarketplaceProductsAroundUser
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetEntities1AroundUserImpl"
private const val LOG_ME = true

class GetMarketplaceProductsAroundUserImpl
@Inject
constructor(
    private val marketplaceProductCacheDataSource: MarketplaceProductCacheDataSource,
    private val marketplaceProductNetworkDataSource: MarketplaceProductNetworkDataSource,
    private val entityFactory: MarketplaceProductFactory
): GetMarketplaceProductsAroundUser {

    override fun getMarketplaceProductsAroundUser(
        location : Location?,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<MarketplaceProduct>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeApiCall(
            Dispatchers.IO,
            networkTimeout = NetworkConstants.NETWORK_TIMEOUT,
            onErrorAction = onErrorAction,
        ){
            syncEntities(location)
        }

        val response = object: ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<MarketplaceProduct>>(
            response = syncedEntities,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<MarketplaceProduct>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(LOG_ME) ALog.d(TAG, "getEntities1AroundUser().handleSuccess(): ")


                if(resultObj == null){
                    if(LOG_ME) ALog.d(TAG, "getEntities1AroundUser(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    returnViewState.searchedEntities1List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
    ) : List<MarketplaceProduct> {
        val firestoreSearchParameters =
            FirestoreEntity1SearchParameters(null, location)
        val networkEntitiesList = getNetworkEntities(firestoreSearchParameters)
        for((index, marketplaceProduct: MarketplaceProduct) in networkEntitiesList.withIndex()) {
            if(LOG_ME)ALog.d(TAG, ".syncEntities(): $index. $marketplaceProduct")
            try {
                marketplaceProductCacheDataSource.insertOrUpdateEntity(marketplaceProduct)
            } catch (e: Exception) {
                ALog.e(TAG, "syncEntities", e)
            }
        }
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(searchParameters : FirestoreEntity1SearchParameters): List<MarketplaceProduct>{
        val networkResult = safeApiCall(Dispatchers.IO){
            marketplaceProductNetworkDataSource.searchEntities(searchParameters)
        }

        val response = object: ApiResponseHandler<List<MarketplaceProduct>, List<MarketplaceProduct>>(
            response = networkResult,
            stateEvent = null
        ){
            override suspend fun handleSuccess(resultObj: List<MarketplaceProduct>): DataState<List<MarketplaceProduct>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully got entities1 around the user."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}
