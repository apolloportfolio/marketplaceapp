package com.aps.catemplateapp.marketplaceapp.di

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductFirestoreService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceOrderFirestoreService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductCategoryFirestoreService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductVendorFirestoreService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.impl.MarketplaceProductFirestoreServiceImpl
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.impl.MarketplaceOrderFirestoreServiceImpl
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.impl.MarketplaceProductCategoryFirestoreServiceImpl
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.impl.MarketplaceProductVendorFirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindMarketplaceProductFirestoreService(implementation: MarketplaceProductFirestoreServiceImpl): MarketplaceProductFirestoreService

    @Binds
    abstract fun bindMarketplaceOrderFirestoreService(implementation: MarketplaceOrderFirestoreServiceImpl): MarketplaceOrderFirestoreService

    @Binds
    abstract fun bindMarketplaceProductCategoryFirestoreService(implementation: MarketplaceProductCategoryFirestoreServiceImpl): MarketplaceProductCategoryFirestoreService

    @Binds
    abstract fun bindMarketplaceProductVendorFirestoreService(implementation: MarketplaceProductVendorFirestoreServiceImpl): MarketplaceProductVendorFirestoreService


}