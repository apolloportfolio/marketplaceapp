package com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction


import com.aps.catemplateapp.common.business.data.cache.StandardCacheDataSource
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct

interface MarketplaceProductCacheDataSource: StandardCacheDataSource<MarketplaceProduct> {
    override suspend fun insertEntity(entity: MarketplaceProduct): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<MarketplaceProduct>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        price: Double?,
        currency: String?,
        brand: String?,
        productName: String?,
        picture2URI: String?,
        picture3URI: String?,
        picture4URI: String?,
        picture5URI: String?,
        picture6URI: String?,
        picture7URI: String?,
        picture8URI: String?,
        picture9URI: String?,
        picture10URI: String?,
        productCategoryID: UniqueID?,
        productVendorID: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceProduct>

    override suspend fun getAllEntities(): List<MarketplaceProduct>

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProduct?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<MarketplaceProduct>): LongArray
}