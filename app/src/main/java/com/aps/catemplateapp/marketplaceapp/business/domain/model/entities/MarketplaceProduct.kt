package com.aps.catemplateapp.marketplaceapp.business.domain.model.entities

import android.os.Parcelable
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.io.Serializable

private const val TAG = "MarketplaceProduct"
private const val LOG_ME = true

// Entity1
@Parcelize
data class MarketplaceProduct(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,

    var price: Double?,
    var currency: String?,
    var brand: String?,
    var productName: String?,
    var picture2URI: String?,
    var picture3URI: String?,
    var picture4URI: String?,
    var picture5URI: String?,
    var picture6URI: String?,
    var picture7URI: String?,
    var picture8URI: String?,
    var picture9URI: String?,
    var picture10URI: String?,
    var productCategoryID: UniqueID?,
    var productVendorID: UniqueID?,

    var latitude: Double?,
    var longitude: Double?,
    var geoLocation: ParcelableGeoPoint?,
    var firestoreGeoLocation: Double?,

    var picture1URI: String?,
    var description: String?,

    var city: String?,

    var ownerID: UserUniqueID?,

    var name: String?,

    var switch1: Boolean? = null,
    var switch2: Boolean? = null,
    var switch3: Boolean? = null,
    var switch4: Boolean? = null,
    var switch5: Boolean? = null,
    var switch6: Boolean? = null,
    var switch7: Boolean? = null,
): Parcelable, Serializable {

    var hasPictures:Boolean =
        picture1URI != null

    @IgnoredOnParcel
    val picture1FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture1ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture1URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture2FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture2ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture2URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture3FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture3ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture3URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture4FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture4ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture4URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture5FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture5ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture5URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture6FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture6ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture6URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture7FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture7ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture7URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture8FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture8ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture8URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture9FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture9ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture9URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture10FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture10ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture10URI!!)
            } else {
                null
            }
        }


    object Entity1Constants {
        const val LOCATION_FIELD_NAME = "firestoreGeoLocation"
    }

    override fun toString(): String {
        return "${id.toString()}: geoLocation == $geoLocation \n"
    }

    fun toIdString() : String {
        return "${this.javaClass.simpleName}($id)"
    }

    companion object {
        const val ownerIDFieldName = "ownerID"
    }
}