package com.aps.catemplateapp.marketplaceapp.business.data.cache.implementation

import com.aps.catemplateapp.common.util.ParcelableGeoPoint
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.cache.abstraction.MarketplaceProductCacheDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProduct
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: MarketplaceProductDaoService
): MarketplaceProductCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceProduct): MarketplaceProduct? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: MarketplaceProduct): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceProduct>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        price: Double?,
        currency: String?,
        brand: String?,
        productName: String?,
        picture2URI: String?,
        picture3URI: String?,
        picture4URI: String?,
        picture5URI: String?,
        picture6URI: String?,
        picture7URI: String?,
        picture8URI: String?,
        picture9URI: String?,
        picture10URI: String?,
        productCategoryID: UniqueID?,
        productVendorID: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,
            price,
            currency,
            brand,
            productName,
            picture2URI,
            picture3URI,
            picture4URI,
            picture5URI,
            picture6URI,
            picture7URI,
            picture8URI,
            picture9URI,
            picture10URI,
            productCategoryID,
            productVendorID,
            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,
            picture1URI,
            description,
            city,
            ownerID,
            name,
            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<MarketplaceProduct> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<MarketplaceProduct> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProduct? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<MarketplaceProduct>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}