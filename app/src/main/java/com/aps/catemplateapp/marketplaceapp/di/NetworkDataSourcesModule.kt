package com.aps.catemplateapp.marketplaceapp.di

import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceOrderNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductCategoryNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductVendorNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.data.network.impl.MarketplaceProductNetworkDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.network.impl.MarketplaceOrderNetworkDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.network.impl.MarketplaceProductCategoryNetworkDataSourceImpl
import com.aps.catemplateapp.marketplaceapp.business.data.network.impl.MarketplaceProductVendorNetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindMarketplaceProductNetworkDataSource(implementation: MarketplaceProductNetworkDataSourceImpl): MarketplaceProductNetworkDataSource

    @Binds
    abstract fun bindMarketplaceOrderNetworkDataSource(implementation: MarketplaceOrderNetworkDataSourceImpl): MarketplaceOrderNetworkDataSource

    @Binds
    abstract fun bindMarketplaceProductCategoryNetworkDataSource(implementation: MarketplaceProductCategoryNetworkDataSourceImpl): MarketplaceProductCategoryNetworkDataSource

    @Binds
    abstract fun bindMarketplaceProductVendorNetworkDataSource(implementation: MarketplaceProductVendorNetworkDataSourceImpl): MarketplaceProductVendorNetworkDataSource

}