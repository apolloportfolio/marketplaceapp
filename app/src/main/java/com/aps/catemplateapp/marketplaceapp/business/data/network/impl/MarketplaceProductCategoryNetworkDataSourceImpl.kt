package com.aps.catemplateapp.marketplaceapp.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductCategoryNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductCategoryFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductCategoryNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: MarketplaceProductCategoryFirestoreService
): MarketplaceProductCategoryNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: MarketplaceProductCategory) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<MarketplaceProductCategory>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: MarketplaceProductCategory) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<MarketplaceProductCategory> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<MarketplaceProductCategory> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<MarketplaceProductCategory>): List<MarketplaceProductCategory>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): MarketplaceProductCategory? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersMarketplaceProductCategory(userId: UserUniqueID): List<MarketplaceProductCategory>? {
        return firestoreService.getUsersMarketplaceProductCategorys(userId)
    }
}