package com.aps.catemplateapp.marketplaceapp.business.domain.model.factories

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceOrder
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceOrderFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        userId: UniqueID?,
        isPaid: Boolean?,
        isCanceled: Boolean?,
        orderedProducts: String?,

        picture1URI: String?,

        name: String?,
        description : String?,

        ownerID: UserUniqueID?,
    ): MarketplaceOrder {
        return MarketplaceOrder(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            userId,
            isPaid,
            isCanceled,
            orderedProducts,

            picture1URI,
            name,
            description,
            ownerID,
        )
    }

    fun createEntitiesList(numEntities: Int): List<MarketplaceOrder> {
        val list: ArrayList<MarketplaceOrder> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): MarketplaceOrder {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<MarketplaceOrder> {
            return arrayListOf(
                MarketplaceOrder(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user01"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Smartphone X, test01, 1, 99.99, USD, 1, Wireless Headphones, test10, 1, 99.99, USD, 2, Fashionable Backpack, test07, 1, 39.99, USD, 2",
                    picture1URI = "1",
                    name = "test01",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user02"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Waterproof Camera, test02, 1, 149.99, USD, 1",
                    picture1URI = "1",
                    name = "test02",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user03"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Organic Food Basket, test03, 1, 199.99, USD, 1",
                    picture1URI = "1",
                    name = "test03",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user04"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Artisanal Cheese Collection, test04, 1, 249.99, USD, 1",
                    picture1URI = "1",
                    name = "test04",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user05"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Fitness Tracker, test05, 1, 299.99, USD, 1",
                    picture1URI = "1",
                    name = "test05",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user06"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Virtual Reality Headset, test06, 1, 349.99, USD, 1",
                    picture1URI = "1",
                    name = "test06",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666"),
                ),
                MarketplaceOrder(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user07"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Fashionable Backpack, test07, 1, 399.99, USD, 1",
                    picture1URI = "1",
                    name = "test07",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user08"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Aromatherapy Diffuser, test08, 1, 449.99, USD, 1",
                    picture1URI = "1",
                    name = "test08",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user09"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Outdoor Camping Tent, test09, 1, 499.99, USD, 1",
                    picture1URI = "1",
                    name = "test09",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                ),
                MarketplaceOrder(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    userId = UniqueID("user10"),
                    isPaid = true,
                    isCanceled = false,
                    orderedProducts = "Wireless Headphones, test10, 1, 99.99, USD, 1",
                    picture1URI = "1",
                    name = "test10",
                    description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sapien arcu, pellentesque id faucibus a, gravida sit amet leo. Quisque aliquam mattis pretium.",
                    ownerID = UserUniqueID("666", "666")
                )
            )
        }
    }
}