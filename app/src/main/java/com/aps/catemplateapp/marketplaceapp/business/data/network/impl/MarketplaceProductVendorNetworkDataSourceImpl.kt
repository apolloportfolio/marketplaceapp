package com.aps.catemplateapp.marketplaceapp.business.data.network.impl

import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.marketplaceapp.business.data.network.abs.MarketplaceProductVendorNetworkDataSource
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs.MarketplaceProductVendorFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductVendorNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: MarketplaceProductVendorFirestoreService
): MarketplaceProductVendorNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: MarketplaceProductVendor) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<MarketplaceProductVendor>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: MarketplaceProductVendor) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<MarketplaceProductVendor> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: MarketplaceProductVendor): MarketplaceProductVendor? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<MarketplaceProductVendor> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<MarketplaceProductVendor>): List<MarketplaceProductVendor>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): MarketplaceProductVendor? {
        return firestoreService.getEntityById(id)
    }
}