package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "entities2")
data class MarketplaceOrderFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("userId")
    @Expose
    var userId: UniqueID?,

    @SerializedName("isPaid")
    @Expose
    var isPaid: Boolean?,

    @SerializedName("isCanceled")
    @Expose
    var isCanceled: Boolean?,

    @SerializedName("orderedProducts")
    @Expose
    var orderedProducts: String?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("name")
    @Expose
    var name: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("ownerID")
    @Expose
    var ownerID: UserUniqueID?,

    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}