package com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.abs

import com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductVendor
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.mappers.MarketplaceProductVendorFirestoreMapper
import com.aps.catemplateapp.marketplaceapp.framework.datasources.networkdatasources.firebase.model.MarketplaceProductVendorFirestoreEntity

interface MarketplaceProductVendorFirestoreService: BasicFirestoreService<
        MarketplaceProductVendor,
        MarketplaceProductVendorFirestoreEntity,
        MarketplaceProductVendorFirestoreMapper
        > {

}