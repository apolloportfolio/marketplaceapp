package com.aps.catemplateapp.marketplaceapp.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.marketplaceapp.MarketplaceAppViewModelFactory
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.marketplaceapp.business.domain.model.factories.MarketplaceProductFactory
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.marketplaceapp.business.interactors.HomeScreenInteractors
import com.aps.catemplateapp.marketplaceapp.framework.presentation.activity01.state.HomeScreenViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object MarketplaceAppViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    @Named("Feature2ViewModelFactory")
    fun provideMarketplaceAppViewModelFactory(
        homeScreenInteractors: HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        marketplaceProductFactory: MarketplaceProductFactory,
        editor: SharedPreferences.Editor,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return MarketplaceAppViewModelFactory(
            homeScreenInteractors = homeScreenInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            marketplaceProductFactory = marketplaceProductFactory,
            editor = editor,
        )
    }
}