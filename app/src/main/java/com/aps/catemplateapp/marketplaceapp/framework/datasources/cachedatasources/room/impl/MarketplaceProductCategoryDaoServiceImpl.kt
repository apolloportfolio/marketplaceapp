package com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.impl

import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.common.util.UniqueID
import com.aps.catemplateapp.common.util.UserUniqueID
import com.aps.catemplateapp.marketplaceapp.business.domain.model.entities.MarketplaceProductCategory
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.abs.MarketplaceProductCategoryDaoService
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductCategoryDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.mappers.MarketplaceProductCategoryCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarketplaceProductCategoryDaoServiceImpl
@Inject
constructor(
    private val dao: MarketplaceProductCategoryDao,
    private val mapper: MarketplaceProductCategoryCacheMapper,
    private val dateUtil: DateUtil
): MarketplaceProductCategoryDaoService {

    override suspend fun insertOrUpdateEntity(entity: MarketplaceProductCategory): MarketplaceProductCategory {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,
                entity.parentId,

                entity.picture1URI,

                entity.name,
                entity.description,

                entity.ownerID,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: MarketplaceProductCategory): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<MarketplaceProductCategory>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): MarketplaceProductCategory? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,
        parentId: UniqueID?,

        picture1URI: String?,

        name: String?,
        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,
                parentId,

                picture1URI,

                name,
                description,

                ownerID,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,
                parentId,

                picture1URI,

                name,
                description,

                ownerID,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<MarketplaceProductCategory>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<MarketplaceProductCategory> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<MarketplaceProductCategory> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}