package com.aps.catemplateapp.common.framework.presentation.views.examples

import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.aps.catemplateapp.common.framework.presentation.views.ToastState
import com.aps.catemplateapp.common.util.ALog

private const val TAG = "ExampleViewModelIntegrationSimpleExternalFun"
private const val LOG_ME = true

@Composable
fun ExampleViewModelIntegrationSimpleExternalFun(
    viewModel: MyViewModel = viewModel()
) {
//    val viewState by viewModel.viewState.observeAsState()

    // This works but only if setViewState function changes the entire viewState to it's copy and
    // not when we only change one parameter of that viewState.
    val viewState by viewModel.getCurrentViewStateOrNewAsLiveData().observeAsState()
    val toastState = viewState?.toastState
    val showToast = viewState?.showToast

    // This works but only if setViewState function changes the entire viewState to it's copy and
    // not when we only change one parameter of that viewState.
//    val viewState by rememberUpdatedState(viewModel.getCurrentViewStateOrNewAsLiveData().observeAsState())
//    val toastStatus = viewState.value?.toastStatus
//    val showToast = viewState.value?.showToast


    val setToastInViewModel: (Boolean) -> Unit = { show ->
        viewModel.setShowToast(show)
    }
    val setToastInViewModelUsingToastState: (Boolean) -> Unit = { show ->
        viewModel.setToastState(
            ToastState(
                show,
                "Toast using ToastStatus",
                updateToastInViewModel = { viewModel.setToast(it!!) },
            )
        )
    }
    ExampleViewModelIntegrationSimple(
        showToast,
        toastState!!,
        setToastInViewModel,
        setToastInViewModelUsingToastState,
    )
}

@Composable
fun ExampleViewModelIntegrationSimple(
    showToast: Boolean?,
    toastState: ToastState,
    setToastInViewModel: (Boolean) -> Unit,
    setToastInViewModelUsingToastState: (Boolean) -> Unit,
) {
    if(LOG_ME)ALog.d(
        TAG, "(): " +
                            "Recomposition.")

    // Click event handler for the button
    val onButtonClick: () -> Unit = {
//        setToastInViewModel(true)
        setToastInViewModelUsingToastState(true)
    }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        Button(
            onClick = onButtonClick,
            modifier = Modifier.padding(16.dp)
        ) {
            Text("Show Toast")
        }
    }

    val context = LocalContext.current

    LaunchedEffect(showToast) {
        if (showToast == true) {
            Toast.makeText(
                context,
                "This is a Toast message using showToast!",
                Toast.LENGTH_SHORT
            ).show()
            setToastInViewModel(false)
        }
    }

    // Won't work without this:
    LaunchedEffect(toastState) {
        if (toastState.show) {
            if(LOG_ME)ALog.d(
                TAG, "(): " +
                                    "Showing Toast with ToastStatus")
            Toast.makeText(
                context,
                "This is a Toast message using toastStatus!",
                Toast.LENGTH_SHORT
            ).show()
            setToastInViewModelUsingToastState(false)
        }
    }
}

class MyViewModel : ViewModel() {

    // MutableLiveData representing the viewState
    private val _viewState = MutableLiveData(
        ViewState(
        false,
        ToastState(
            false,
            "Toast",
            updateToastInViewModel = { this.setToast(it!!) },
        )
    )
    )
    val viewState: LiveData<ViewState> get() = _viewState

    fun setShowToast(showToast: Boolean) {
        // This works but test the others again
//        val currentState = _viewState.value
//        _viewState.value = currentState?.copy(showToast = showToast)

//         This doesn't work
//        val update = getCurrentViewStateOrNew()
//        setViewState(update)

//         This doesn't work
//        val update = getCurrentViewStateOrNew_()
//        update.showToast = showToast
//        setViewState(update)

//         This doesn't work
//        val update = getCurrentViewStateOrNewWithoutInit()
//        update.showToast = showToast
//        setViewState(update)

        // This works
//        val update = getCurrentViewStateOrNewWithoutInit()
//        setViewState(update.copy(showToast = showToast))

        // This works
        val update = getCurrentViewStateOrNew()
        setViewState(update.copy(showToast = showToast))
    }

    fun setToastState(toastState: ToastState) {
        if(LOG_ME) ALog.d(
            TAG, "setToastStatus(): " +
                    "Setting toastStatus to ${toastState.message}, " +
                    "visibility == ${toastState.show}"
        )
        if(LOG_ME)ALog.d(
            TAG, "(): " +
                "this._viewState.value before change == ${System.identityHashCode(this._viewState.value)}\n" +
                "this._viewState.value before change == ${this._viewState.value}")

//         This doesn't work
//        toastStatus.updateToastInViewModel = { status ->
//            setToast(status!!)
//        }
//        val update = getCurrentViewStateOrNew()
//        update.toastStatus = toastStatus
//        setViewState(update)

        // This works
//        val update = getCurrentViewStateOrNew()
//        setViewState(update.copy(toastStatus = toastStatus))

//         This doesn't work
//        this._viewState.value?.toastStatus?.show = true

//         This doesn't work
//        this._viewState.value!!.toastStatus = toastStatus

        // This works
        this._viewState.value = this._viewState.value!!.copy(toastState = toastState)



        if(LOG_ME)ALog.d(
            TAG, "(): " +
                "this._viewState.value after change == ${System.identityHashCode(this._viewState.value)}\n" +
                "this._viewState.value after change == ${this._viewState.value}")
    }

    fun getCurrentViewStateOrNew(): ViewState {
        return viewState.value ?: initNewViewState()
    }

    fun getCurrentViewStateOrNew_(): ViewState {
        return _viewState.value ?: initNewViewState()
    }

    fun getCurrentViewStateOrNewWithoutInit(): ViewState {
        return _viewState.value!!
    }

    fun setToast(toastState: ToastState) {
        ALog.d(
            TAG, "setToast(): " +
                    "Setting toastStatus to ${toastState.message}, " +
                    "visibility == ${toastState.show}"
        )
        toastState.updateToastInViewModel = { status ->
            setToast(status!!)
        }
        val update = getCurrentViewStateOrNew()
        update.toastState = toastState
        setViewState(update)
    }
    
    private fun initNewViewState(): ViewState {
        val newInstance: ViewState = ViewState(
            showToast = false,
            toastState = ToastState(
                false,
                "Toast",
                updateToastInViewModel = { this.setToast(it!!) },
            )
        )
        newInstance.toastState = ToastState(
            updateToastInViewModel = {
                setToast(it!!)
            }
        )

        return newInstance
    }

    fun getCurrentViewStateOrNewAsLiveData(): LiveData<ViewState>{
        if(viewState.value == null) setViewState(initNewViewState())
        return viewState
    }

    fun setViewState(viewState: ViewState){
        if(LOG_ME)ALog.d(
            TAG, ".setViewState(): " +
                                "viewState before update == ${_viewState.value}")
        _viewState.value = viewState
        if(LOG_ME)ALog.d(
            TAG, ".setViewState(): " +
                "viewState after update == ${_viewState.value}")
    }
}

// ViewState data class representing the state of the screen
data class ViewState(
    var showToast: Boolean = false,
    var toastState: ToastState,//? = null,
)