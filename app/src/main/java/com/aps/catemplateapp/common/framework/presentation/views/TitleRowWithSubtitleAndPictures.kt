package com.aps.catemplateapp.common.framework.presentation.views

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme
import com.aps.catemplateapp.common.util.ALog
import com.aps.catemplateapp.common.util.composableutils.fallbackPainterResource
import com.google.firebase.storage.StorageReference

private const val TAG = "TitleRowWithSubtitleAndPictures"
private const val LOG_ME = true

@Composable
fun TitleRowWithSubtitleAndPictures(
    titleString: String?,
    subtitleString: String? = null,
    textAlign: TextAlign = TextAlign.Start,
    onClick: (() -> Unit)? = null,
    leftPictureItemRef: StorageReference? = null,
    rightPictureItemRef: StorageReference? = null,
    leftPicturePainter: Painter? = null,
    rightPicturePainter: Painter? = null,
    leftPictureContentDescription: String = stringResource(id = R.string.left_image),
    rightPictureContentDescription: String = stringResource(id = R.string.right_image),
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int? = R.drawable.list_item_example_background_1,
    cardPaddingValues: PaddingValues = PaddingValues(
        start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
        end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
        top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
        bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
    ),
    internalPaddingValues: PaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
    titleFontSize: Int = 24,
    subtitleFontSize: Int = 18,
    leftImageTint: Color = MaterialTheme.colors.onSurface,
    rightImageTint: Color = MaterialTheme.colors.onSurface,
    titleColor: Color = MaterialTheme.colors.onSurface,
    subtitleColor: Color = MaterialTheme.colors.onSurface,
    leftPictureOnClick: () -> Unit = {},
    rightPictureOnClick: () -> Unit = {},
    @SuppressLint("ModifierParameter") modifier: Modifier = Modifier,
) {
    if(LOG_ME) ALog.d(TAG, "$TAG(): Recomposition start: $titleString")

    val transparentColor = remember {
        Color(0x00000000)
    }
    val cardOnClick: () -> Unit = if(onClick != null) { onClick } else { {} }
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(cardPaddingValues)
            .background(transparentColor)
            .clickable { cardOnClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(internalPaddingValues)
            ) {

                val (leftImage, title, subtitle, rightImage, titlesSpacer,
                ) = createRefs()

                val leftImagePresent = leftPictureItemRef != null || leftPicturePainter != null
                val rightImagePresent = rightPictureItemRef != null || rightPicturePainter != null
                val recalculatedTitleFontSize: Int = if(subtitleString == null) {
                    (titleFontSize*1.3).toInt()
                } else {
                    titleFontSize
                }
                val leftPictureConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
                val rightPictureConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    end.linkTo(parent.end)
                }

                if(leftPictureItemRef != null) {
                    // Image to the start.
                    ImageFromFirebase(
                        leftPictureItemRef,
                        modifier = Modifier
                            .constrainAs(leftImage, leftPictureConstrain)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(internalPaddingValues)
                            .clickable { leftPictureOnClick() },
                        contentDescription = leftPictureContentDescription,
                        contentScale = ContentScale.Fit
                    )
                } else if(leftPicturePainter != null) {
                    Image(
                        painter = leftPicturePainter,
                        contentDescription = leftPictureContentDescription,
                        modifier = Modifier
                            .constrainAs(leftImage, leftPictureConstrain)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(internalPaddingValues)
                            .clickable { leftPictureOnClick() },
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = ColorFilter.tint(leftImageTint),
                    )
                }


                // Title
                Text(
                    modifier = Modifier
                        .constrainAs(title) {
                            top.linkTo(parent.top)
                            bottom.linkTo(
                                if (subtitleString != null) {
                                    subtitle.top
                                } else {
                                    parent.bottom
                                }
                            )
                            start.linkTo(
                                if (leftImagePresent) {
                                    leftImage.end
                                } else {
                                    parent.start
                                }
                            )
                            end.linkTo(
                                if (rightImagePresent) {
                                    rightImage.start
                                } else {
                                    parent.end
                                }
                            )
                            width = Dimension.fillToConstraints
                        }
                        .padding(
                            start = internalPaddingValues.calculateStartPadding(LocalLayoutDirection.current),
                            top = internalPaddingValues.calculateTopPadding(),
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                        ),
                    text = titleString ?: "",
                    maxLines = 3,
                    textAlign = textAlign,
                    style = TextStyle(
                        color = titleColor,
                        fontWeight = FontWeight.Bold,
                        fontSize = recalculatedTitleFontSize.sp,
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                // Subtitle
                if(subtitleString != null) {
                    Spacer(modifier = Modifier
                        .constrainAs(titlesSpacer) {
                            top.linkTo(title.bottom)
                            bottom.linkTo(subtitle.top)
                            start.linkTo(
                                if (leftImagePresent) {
                                    leftImage.end
                                } else {
                                    parent.start
                                }
                            )
                            end.linkTo(
                                if (rightImagePresent) {
                                    rightImage.start
                                } else {
                                    parent.end
                                }
                            )
                        }
                        .width(dimensionResource(id = R.dimen.margin_between_text_views)))

                    Text(
                        modifier = Modifier
                            .constrainAs(subtitle) {
                                top.linkTo(title.bottom)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(
                                    if (leftImagePresent) {
                                        leftImage.end
                                    } else {
                                        parent.start
                                    }
                                )
                                end.linkTo(
                                    if (rightImagePresent) {
                                        rightImage.start
                                    } else {
                                        parent.end
                                    }
                                )
                                width = Dimension.fillToConstraints
                            }
                            .padding(internalPaddingValues)
                            .background(transparentColor),
                        text = subtitleString,
                        textAlign = textAlign,
                        style = TextStyle(
                            color = subtitleColor,
                            fontWeight = FontWeight.Bold,
                            fontSize = subtitleFontSize.sp,
                        ),
                        maxLines = 3,
                    )
                }

                // Image to the end.
                if(rightPictureItemRef != null) {
                    ImageFromFirebase(
                        rightPictureItemRef,
                        modifier = Modifier
                            .constrainAs(rightImage, rightPictureConstrain)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(internalPaddingValues)
                            .clickable { rightPictureOnClick() },
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )
                } else if(rightPicturePainter != null) {
                    Image(
                        painter = rightPicturePainter,
                        contentDescription = rightPictureContentDescription,
                        modifier = Modifier
                            .constrainAs(rightImage, rightPictureConstrain)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(internalPaddingValues)
                            .clickable { rightPictureOnClick() },
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = ColorFilter.tint(rightImageTint),
                    )
                }

            }
        }
    }
    if(LOG_ME) ALog.d(TAG, "$TAG(): Recomposition End: $titleString")

}

@Preview(name = "Title&Subtitle, no picture, aligned start.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview1() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = "The Subtitle",
            textAlign = TextAlign.Start,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = null,
            rightPicturePainter = null,
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Title&Subtitle, no picture, aligned center.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview2() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = "The Subtitle",
            textAlign = TextAlign.Center,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = null,
            rightPicturePainter = null,
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Just Title, no picture, aligned start.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview3() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = null,
            textAlign = TextAlign.Start,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = null,
            rightPicturePainter = null,
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Just Title, no picture, aligned center.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview4() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = null,
            textAlign = TextAlign.Center,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = null,
            rightPicturePainter = null,
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Title&Subtitle, aligned start.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview5() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = "The Subtitle",
            textAlign = TextAlign.Start,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = fallbackPainterResource(R.drawable.ic_launcher),
            rightPicturePainter = fallbackPainterResource(R.drawable.ic_back),
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Title&Subtitle, aligned center.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview6() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = "The Subtitle",
            textAlign = TextAlign.Center,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = fallbackPainterResource(R.drawable.ic_launcher),
            rightPicturePainter = fallbackPainterResource(R.drawable.ic_back),
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Just Title, aligned start.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview7() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = null,
            textAlign = TextAlign.Start,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = fallbackPainterResource(R.drawable.ic_launcher),
            rightPicturePainter = fallbackPainterResource(R.drawable.ic_back),
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Just Title, aligned center.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview8() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = null,
            textAlign = TextAlign.Center,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = fallbackPainterResource(R.drawable.ic_launcher),
            rightPicturePainter = fallbackPainterResource(R.drawable.ic_back),
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Just Title, only left picture, aligned center.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview9() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = null,
            textAlign = TextAlign.Center,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = fallbackPainterResource(R.drawable.ic_launcher),
            rightPicturePainter = null,
//            backgroundPainter = fallbackPainterResource(id = R.drawable.list_item_example_background_1),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}

@Preview(name = "Just Title, only left picture, aligned center, DefaultBackground.")
@Composable
fun TitleRowWithSubtitleAndPicturesPreview10() {
    CommonTheme {
        TitleRowWithSubtitleAndPictures(
            titleString = "The Title",
            subtitleString = null,
            textAlign = TextAlign.Center,
            onClick = {},
            leftPictureItemRef = null,
            rightPictureItemRef = null,
            leftPicturePainter = fallbackPainterResource(R.drawable.ic_launcher),
            rightPicturePainter = null,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundTitleType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = null,
            ),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            cardPaddingValues = PaddingValues(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            ),
            internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        )
    }
}