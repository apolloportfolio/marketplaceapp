package com.aps.catemplateapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccessTime
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme

@Composable
fun TimeInputType01(
    selectedTime: String,
    onTimeSelected: (String) -> Unit,
    showTimePicker: ()-> Unit,
    backgroundDrawableId: Int? = null,
    composableBackground: DrawnBackground? = null,
    modifier: Modifier = Modifier
) {
    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {
        var outlinedTextFieldModifier: Modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
            .clip(MaterialTheme.shapes.medium)
            .padding(PaddingValues(4.dp, 4.dp, 4.dp, 4.dp))
        if(backgroundDrawableId == null && composableBackground == null) {
            outlinedTextFieldModifier = outlinedTextFieldModifier
                .background(MaterialTheme.colors.surface)
        }

        OutlinedTextField(
            value = selectedTime,
            onValueChange = { onTimeSelected(it) },
            label = { Text(stringResource(id = R.string.select_time)) },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.AccessTime,
                    contentDescription = stringResource(id = R.string.time_icon)
                )
            },
            keyboardOptions = KeyboardOptions(
                capitalization = KeyboardCapitalization.None,
                keyboardType = KeyboardType.Text
            ),
            modifier = outlinedTextFieldModifier,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                cursorColor = MaterialTheme.colors.primary
            ),
            singleLine = true,
            trailingIcon = {
                IconButton(onClick = showTimePicker) {
                    Icon(
                        imageVector = Icons.Default.AccessTime,
                        contentDescription = stringResource(id = R.string.time_icon)
                    )
                }
            },
            maxLines = 1
        )
    }
}

@Preview
@Composable
private fun TimeInputType01Preview01() {
    CommonTheme {
        TimeInputType01(
            selectedTime = "",
            onTimeSelected = {},
            showTimePicker = {},
            modifier = Modifier,
        )
    }
}

@Preview
@Composable
private fun TimeInputType01Preview02() {
    CommonTheme {
        TimeInputType01(
            selectedTime = "",
            onTimeSelected = {},
            showTimePicker = {},
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundTitleType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
            modifier = Modifier,
        )
    }
}