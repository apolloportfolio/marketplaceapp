package com.aps.catemplateapp.common.framework.presentation.views

import android.os.Parcelable
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

private const val TAG = "CustomProgressIndicator"
private const val LOG_ME = false

// References:
// https://semicolonspace.com/jetpack-compose-circularprogressindicator/
// https://www.jetpackcompose.net/jetpack-compose-progress-indicator-progressbar
@Composable
internal fun ShowProgressIndicatorIfNecessary(
    progressIndicatorState: ProgressIndicatorState?,
    modifier: Modifier,
) {
    var firstComposition by rememberSaveable { mutableStateOf(true) }
    var resetProgressIndicator by remember { mutableStateOf(false) }

    if(LOG_ME) ALog.d(TAG, "ShowProgressIndicatorIfNecessary():\n" +
            "resetProgressIndicator == $resetProgressIndicator\n" +
            "progressIndicatorState == $progressIndicatorState"
    )


    var showProgressIndicator by remember { mutableStateOf(false) }
    showProgressIndicator = progressIndicatorState?.show == true
    if (showProgressIndicator) {
        Box(modifier = modifier) {
            when(progressIndicatorState?.type) {

                ProgressIndicatorType.IndeterminateCircularProgressIndicator -> {
                    if(LOG_ME) ALog.d(TAG, "ShowCircularProgressIndicatorIfNecessary(): " +
                            "IndeterminateCircularProgressIndicator")
                    CircularProgressIndicator(
                        modifier = Modifier.size(size = 64.dp),
                        color = MaterialTheme.colors.primary,
                        strokeWidth = 6.dp
                    )
                }

                ProgressIndicatorType.DeterminateCircularProgressIndicator -> {
                    if(LOG_ME) ALog.d(TAG, "ShowCircularProgressIndicatorIfNecessary(): " +
                            "DeterminateCircularProgressIndicator")
                    val progressValue = progressIndicatorState.progress
                    val infiniteTransition = rememberInfiniteTransition(label = "DeterminateCircularProgressIndicator")

                    val progressAnimationValue by infiniteTransition.animateFloat(
                        initialValue = 0.0f,
                        targetValue = progressValue,
                        animationSpec = infiniteRepeatable(animation = tween(900)), label = ""
                    )

                    CircularProgressIndicator(
                        modifier = Modifier.size(size = 64.dp),
                        color = MaterialTheme.colors.primary,
                        strokeWidth = 6.dp,
                        progress = progressAnimationValue,
                    )
                }

                ProgressIndicatorType.IndeterminateLinearProgressIndicator -> {
                    if(LOG_ME) ALog.d(TAG, "ShowCircularProgressIndicatorIfNecessary(): " +
                            "IndeterminateLinearProgressIndicator")
                    LinearProgressIndicator(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(15.dp),
                        backgroundColor = MaterialTheme.colors.background,
                        color = MaterialTheme.colors.primary,
                    )
                }

                ProgressIndicatorType.DeterminateLinearProgressIndicator -> {
                    if(LOG_ME) ALog.d(TAG, "ShowCircularProgressIndicatorIfNecessary(): " +
                            "DeterminateLinearProgressIndicator")
                    LinearProgressIndicator(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(15.dp),
                        backgroundColor = MaterialTheme.colors.background,
                        color = MaterialTheme.colors.primary,
                        progress = progressIndicatorState.progress
                    )
                }

                else -> {
                    if(LOG_ME) ALog.d(TAG, "ShowCircularProgressIndicatorIfNecessary(): " +
                            "Undefined ProgressIndicatorType\n" +
                            "progressIndicatorState == $progressIndicatorState")
                }
            }

        }
    } else {
        if(LOG_ME)ALog.d(TAG, "ShowProgressIndicatorIfNecessary(): " +
                                "Hiding progress indicator.")
    }

    LaunchedEffect(resetProgressIndicator) {
        if(LOG_ME) ALog.d(TAG,
            "ShowProgressIndicatorIfNecessary().LaunchedEffect(resetProgressIndicator): " +
                    "Recomposition start")
        if(firstComposition) {
            if(LOG_ME) ALog.d(TAG,
                "ShowProgressIndicatorIfNecessary()." +
                        "LaunchedEffect(resetProgressIndicator): " +
                        "This is the first composition.")
            firstComposition = false
            resetProgressIndicator = false
        } else {
            if (resetProgressIndicator) {
                progressIndicatorState?.updateProgressIndicatorStateInViewModel?.invoke(
                    ProgressIndicatorState()
                )
            }
            resetProgressIndicator = false
        }
        if(LOG_ME) ALog.d(TAG, "ShowProgressIndicatorIfNecessary()." +
                "LaunchedEffect(resetProgressIndicator): Recomposition end")
    }

    if(LOG_ME) ALog.d(TAG,
        "ShowProgressIndicatorIfNecessary(): Recomposition end")
}

enum class ProgressIndicatorType {
    IndeterminateCircularProgressIndicator,
    DeterminateCircularProgressIndicator,
    IndeterminateLinearProgressIndicator,
    DeterminateLinearProgressIndicator,
}

@Parcelize
data class ProgressIndicatorState(
    var show: Boolean = false,
    var progress: Float = 0.0f,
    var type: ProgressIndicatorType = ProgressIndicatorType.IndeterminateCircularProgressIndicator,
    var updateProgressIndicatorStateInViewModel: (ProgressIndicatorState?) -> Unit = {},
) : Parcelable, Serializable {

    fun shallowCopy(): ProgressIndicatorState {
        return ProgressIndicatorState(
            this.show,
            this.progress,
            this.type,
            this.updateProgressIndicatorStateInViewModel,
        )
    }

    override fun toString(): String {
        return buildString {
            appendLine(
                "ProgressIndicatorState: type: ${type.name} show: $show    progress: $progress"
            )
        }
    }
}

//========================================================================================================
// Reminder: In preview window click on "Start Animation Preview" over each preview, to see it animate.
@Preview
@Composable
internal fun ShowProgressIndicatorIfNecessaryIndeterminateCircularProgressIndicatorPreview() {
    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            painterResource(id = R.drawable.example_background_1),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .matchParentSize()
                .zIndex(0.1f)
        )

        ShowProgressIndicatorIfNecessary(
            progressIndicatorState = ProgressIndicatorState(
                show = true,
                progress = 0.6f,
                type = ProgressIndicatorType.IndeterminateCircularProgressIndicator,
                updateProgressIndicatorStateInViewModel = {},
            ),
            modifier = Modifier
                .align(Alignment.Center)
                .zIndex(0.9f)
        )
    }
}

@Preview
@Composable
internal fun ShowProgressIndicatorIfNecessaryDeterminateCircularProgressIndicatorPreview() {
    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            painterResource(id = R.drawable.example_background_1),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .matchParentSize()
                .zIndex(0.1f)
        )

        ShowProgressIndicatorIfNecessary(
            progressIndicatorState = ProgressIndicatorState(
                show = true,
                progress = 0.6f,
                type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
                updateProgressIndicatorStateInViewModel = {},
            ),
            modifier = Modifier
                .align(Alignment.Center)
                .zIndex(0.9f)
        )
    }
}

@Preview
@Composable
internal fun ShowProgressIndicatorIfNecessaryIndeterminateLinearProgressIndicatorPreview() {
    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            painterResource(id = R.drawable.example_background_1),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .matchParentSize()
                .zIndex(0.1f)
        )

        ShowProgressIndicatorIfNecessary(
            progressIndicatorState = ProgressIndicatorState(
                show = true,
                progress = 0.6f,
                type = ProgressIndicatorType.IndeterminateLinearProgressIndicator,
                updateProgressIndicatorStateInViewModel = {},
            ),
            modifier = Modifier
                .align(Alignment.Center)
                .zIndex(0.9f)
        )
    }
}

@Preview
@Composable
internal fun ShowProgressIndicatorIfNecessaryDeterminateLinearProgressIndicatorPreview() {
    Box(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            painterResource(id = R.drawable.example_background_1),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .matchParentSize()
                .zIndex(0.1f)
        )

        ShowProgressIndicatorIfNecessary(
            progressIndicatorState = ProgressIndicatorState(
                show = true,
                progress = 0.6f,
                type = ProgressIndicatorType.DeterminateLinearProgressIndicator,
                updateProgressIndicatorStateInViewModel = {},
            ),
            modifier = Modifier
                .align(Alignment.Center)
                .zIndex(0.9f)
        )
    }
}