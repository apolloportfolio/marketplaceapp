package com.aps.catemplateapp.common.util.extensions

import com.ckdroid.geofirequery.GeoQuery
import com.ckdroid.geofirequery.model.Distance
import com.ckdroid.geofirequery.model.QueryLocation
import com.ckdroid.geofirequery.utils.BoundingBoxUtils
import com.google.firebase.firestore.Query

fun GeoQuery.whereIn(field: String, value: Any): GeoQuery {
    this.query = this.query.whereArrayContains(field, value)
    return this
}

fun Query.whereNearToLocation(
    queryAtLocation: QueryLocation,
    distance: Distance,
    fieldName: String = QueryLocation.DEFAULT_KEY_GEO_DEGREE_MATCH
): Query {
    val geoPointUtils = BoundingBoxUtils(distance.unit)
    val boundingBox = geoPointUtils.getBoundingBox(queryAtLocation, distance.distance)
    return orderBy(fieldName)
        .whereGreaterThanOrEqualTo(fieldName, boundingBox.minimumMatch)
        .whereLessThanOrEqualTo(fieldName, boundingBox.maximumMatch)
}