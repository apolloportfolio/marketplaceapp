package com.aps.catemplateapp.common.business.domain.state

import com.aps.catemplateapp.common.util.ALog
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.*

private const val TAG = "DataChannelManager"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
abstract class DataChannelManager<ViewState> {

    private var channelScope: CoroutineScope? = null
    private val stateEventManager: StateEventManager = StateEventManager()

    val messageStack = MessageStack()

    val shouldDisplayProgressBar = stateEventManager.shouldDisplayProgressBar

    fun setupChannel(){
        cancelJobs()
    }

    abstract fun handleNewData(data: ViewState)

    fun launchJob(
        stateEvent: StateEvent,
        jobFunction: Flow<DataState<ViewState>?>,
        handleStateMessage: Boolean = true,
    ){
        val methodName = "launchJob"
        val canExecuteNewStateEvent = canExecuteNewStateEvent(stateEvent)
        if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                "stateEvent name == ${stateEvent.eventName()}    " +
                "handleStateMessage == $handleStateMessage")
        if(!canExecuteNewStateEvent) {
            ALog.w(
                TAG, "$methodName(): " +
                    "Can't execute new state event: ${stateEvent.eventName()}")
            return
        }

        if(LOG_ME)ALog.d(
            TAG, "$methodName(): " +
                "launching job: ${stateEvent.eventName()}")
        addStateEvent(stateEvent)
        jobFunction
            .onEach { dataState ->
                dataState?.let { dState ->
                    withContext(Main){
                        dataState.data?.let { data ->
                            if(LOG_ME) ALog.d(
                                TAG, ".$methodName(): " +
                                    "handling new data.")
                            handleNewData(data)
                        }
                        dataState.stateMessage?.let { stateMessage ->
                            if(handleStateMessage) {
                                if(LOG_ME) ALog.d(
                                    TAG, ".$methodName(): " +
                                            "handling stateMessage: ${stateMessage.response.message}")
                                handleNewStateMessage(stateMessage)
                            } else {
                                if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                                                        "handleStateMessage == false")
                            }
                        }
                        dataState.stateEvent?.let { stateEvent ->
                            if(LOG_ME) ALog.d(
                                TAG, ".$methodName(): " +
                                    "removing stateEvent: ${stateEvent.eventName()}")
//                            if(LOG_ME){
//                                ALog.d(TAG, ".$methodName(): " +
//                                        "activeStateEvents before removal:\n" +
//                                        stateEventManager.printStateEvents()
//                                )
//                                ALog.d(TAG, ".$methodName(): " +
//                                        "messageStack before removal:\n" +
//                                        "${printStateMessages()}")
//                            }
                            removeStateEvent(stateEvent)
//                            if(LOG_ME){
//                                ALog.d(TAG, ".$methodName(): " +
//                                        "activeStateEvents after removal:\n" +
//                                        stateEventManager.printStateEvents()
//                                )
//                                ALog.d(TAG, ".$methodName(): " +
//                                        "messageStack after removal:\n" +
//                                        "${printStateMessages()}")
//                            }
                        }
                    }
                }
            }
            .launchIn(getChannelScope())
    }

    private fun canExecuteNewStateEvent(stateEvent: StateEvent): Boolean{
        val methodName = "canExecuteNewStateEvent"
        // If a job is already active, do not allow duplication
        if(isJobAlreadyActive(stateEvent)){
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "Can't execute job because it is already active.")
            return false
        }
        // if a dialog is showing, do not allow new StateEvents
        if(!isMessageStackEmpty()){
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "Can't execute job because message stack is not empty. " +
                    "Message stack contents:\n${messageStack.printMessageStackContents()}")

            return false
        }
        return true
    }

    fun isMessageStackEmpty(): Boolean {
        return messageStack.isStackEmpty()
    }

    private fun handleNewStateMessage(stateMessage: StateMessage){
        appendStateMessage(stateMessage)
    }

    private fun appendStateMessage(stateMessage: StateMessage) {
        if(LOG_ME)ALog.d(TAG, "appendStateMessage(): " +
                                "Adding state message: $stateMessage")
        messageStack.add(stateMessage)
    }

    fun clearStateMessage(index: Int = 0){
        if(LOG_ME){
            ALog.d(TAG, ".clearStateMessage(): clear state message")
            val removedStateMessage = messageStack[index]
            ALog.d(
                TAG, "clearStateMessage(): " +
                    "Clearing state message: ${removedStateMessage.response.message}")
        }
        messageStack.removeAt(index)
    }

    fun clearStateMessageWithSpecifiedUIComponent(uiComponent: UIComponentType) {
        val methodName: String = "clearStateMessageWithSpecifiedUIComponent"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(LOG_ME)ALog.d(TAG, "$methodName(): uiComponent == $uiComponent ")

            var index = 0
            for(stateMessage in messageStack) {
                if(stateMessage.response.uiComponentType.equalsByValue(uiComponent)) {
                    if(LOG_ME)ALog.d(
                        TAG, "$methodName(): " +
                            "clearing state message with ui component: " +
                            "${stateMessage.response.uiComponentType}")
                    clearStateMessage(index)
                    break
                }
                index++
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    fun clearAllStateMessages() {
        if(LOG_ME)ALog.d(TAG, "clearAllStateMessages(): " +
                                "Clearing message stack: ${messageStack.printMessageStackContents()}")
        messageStack.clear()
    }

    fun printStateMessages(){
        for(message in messageStack){
            ALog.d(TAG, ".printStateMessages(): ${message.response.message}")
        }
    }

    // for debugging
    fun getActiveJobs() = stateEventManager.getActiveJobNames()

    fun clearActiveStateEventCounter()
            = stateEventManager.clearActiveStateEventCounter()

    fun addStateEvent(stateEvent: StateEvent)
            = stateEventManager.addStateEvent(stateEvent)

    fun removeStateEvent(stateEvent: StateEvent?)
            = stateEventManager.removeStateEvent(stateEvent)

    private fun isStateEventActive(stateEvent: StateEvent)
            = stateEventManager.isStateEventActive(stateEvent)

    fun isJobAlreadyActive(stateEvent: StateEvent): Boolean {
        return isStateEventActive(stateEvent)
    }

    fun getChannelScope(): CoroutineScope {
        return channelScope?: setupNewChannelScope(CoroutineScope(IO))
    }

    private fun setupNewChannelScope(coroutineScope: CoroutineScope): CoroutineScope{
        channelScope = coroutineScope
        return channelScope as CoroutineScope
    }

    fun cancelJobs(){
        if(channelScope != null){
            if(channelScope?.isActive == true){
                channelScope?.cancel()
            }
            channelScope = null
        }
        clearActiveStateEventCounter()
    }

}























