package com.aps.catemplateapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.framework.presentation.values.ColorTransparent
import com.aps.catemplateapp.common.framework.presentation.values.CommonTheme

private const val TAG = "RangeRow"
private const val LOG_ME = true
const val RangeRowTestTag = TAG
const val RangeRowRangeNameTestTag = "_RangeRowRangeNameTestTag"
const val RangeRowRangeMinTestTag = "_RangeRowRangeMinTestTag"
const val RangeRowRangeMaxTestTag = "_RangeRowRangeMaxTestTag"

@Composable
fun RangeRow(
    rangeNameID: Int,
    min: Double?,
    max: Double?,
    updateMin: (Double?) -> Unit,
    updateMax: (Double?) -> Unit,
    testTagPrefix: String = RangeRowTestTag,
    spaceBetweenRanges: Dp = 0.dp,
) {
    ConstraintLayout (
        modifier = Modifier
            .fillMaxWidth()
            .background(ColorTransparent)
    ){
        val focusRequesterMin = remember { FocusRequester() }
        val focusRequesterMax = remember { FocusRequester() }
        val centerGuideline = createGuidelineFromStart(0.5f)
        val (
            title,
            valueMin,
            spacer,
            valueMax,
        ) = createRefs()

        Text(
            text = stringResource(rangeNameID),
            modifier = Modifier
                .testTag(testTagPrefix+RangeRowRangeNameTestTag)
                .constrainAs(title) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }
                .fillMaxWidth(0.3f)
                .padding(
                    start = dimensionResource(R.dimen.button_vertical_margin),
                    top = dimensionResource(R.dimen.rounded_corner_margin),
                    end = dimensionResource(R.dimen.button_horizontal_margin),
                    bottom = dimensionResource(R.dimen.button_vertical_margin),
                )
            ,
            maxLines = 2,
        )

        TextField(
            value = min?.toString() ?: "",
            onValueChange = { newValue ->
                updateMin(newValue.toDoubleOrNull())
            },
            modifier = Modifier
                .testTag(testTagPrefix+RangeRowRangeMinTestTag)
                .constrainAs(valueMin) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    end.linkTo(centerGuideline, margin = spaceBetweenRanges)
                }
                .fillMaxWidth(0.2f)
                .focusRequester(focusRequesterMin),
            singleLine = true,
            textStyle = TextStyle(textAlign = TextAlign.End),
        )

        TextField(
            value = max?.toString() ?: "",
            onValueChange = { newValue ->
                updateMax(newValue.toDoubleOrNull())
            },
            modifier = Modifier
                .testTag(testTagPrefix+RangeRowRangeMaxTestTag)
                .constrainAs(valueMax) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(centerGuideline, margin = spaceBetweenRanges)
                }
                .fillMaxWidth(0.2f)
                .focusRequester(focusRequesterMax),
            singleLine = true,
            textStyle = TextStyle(textAlign = TextAlign.Start),
        )

        LaunchedEffect(Unit) {
            focusRequesterMin.requestFocus()
            focusRequesterMax.requestFocus()
        }
    }
}

//========================================================================================
@Preview
@Composable
internal fun RangeRowPreview() {
    CommonTheme {
        RangeRow(
            rangeNameID = R.string.example_range_1_lbl,
            min = 50.0,
            max = 100.0,
            updateMin = {},
            updateMax = {},
        )
    }
}