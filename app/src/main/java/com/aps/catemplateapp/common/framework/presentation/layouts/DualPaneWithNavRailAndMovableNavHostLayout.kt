package com.aps.catemplateapp.common.framework.presentation.layouts

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.aps.catemplateapp.common.business.domain.state.StateEvent
import com.aps.catemplateapp.common.framework.presentation.DrawerData
import com.aps.catemplateapp.common.framework.presentation.NavItem
import com.aps.catemplateapp.common.framework.presentation.views.NavRailWithSelector
import com.aps.catemplateapp.common.util.ALog

private const val TAG = "DualPaneWithNavRailAndMovableNavHostLayout"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DualPaneWithNavRailAndMovableNavHostLayout(
    launchStateEvent: (StateEvent) -> Unit = {},
    launchStateEventSetDrawerOpen: (Boolean?) -> Unit,

    leftPaneNavController: NavHostController,
    rightPaneNavController: NavHostController,
    leftPaneNavItems: List<NavItem>,
    rightPaneNavItems: List<NavItem>,
    leftNavHost: @Composable () -> Unit,
    rightNavHost: @Composable () -> Unit,
    placeNavRailOnLeftSide: Boolean = true,

    topBar:  @Composable () -> Unit = {},
    drawerData: DrawerData? = null,

    isDrawerOpen: Boolean? = null,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    val drawerBackgroundColor = drawerData?.drawerBackgroundColor ?: MaterialTheme.colors.surface
    val scaffoldState = rememberScaffoldState()
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = topBar,
        drawerContent = drawerData?.drawerContent,
        drawerGesturesEnabled = scaffoldState.drawerState.isOpen,
        drawerShape = drawerData?.drawerShape ?: MaterialTheme.shapes.large,
        drawerElevation = drawerData?.drawerElevation ?: DrawerDefaults.Elevation,
        drawerBackgroundColor = drawerBackgroundColor,
        drawerContentColor = drawerData?.drawerContentColor ?: contentColorFor(drawerBackgroundColor),
        drawerScrimColor = drawerData?.drawerScrimColor ?: DrawerDefaults.scrimColor,
    ) {it
        Row(
            Modifier
                .fillMaxSize()
        ) {
            if(placeNavRailOnLeftSide)NavRailWithSelector(
                leftPaneNavController = leftPaneNavController,
                rightPaneNavController = rightPaneNavController,
                navItems = leftPaneNavItems,
            )
            Box(modifier = Modifier
                .weight(1f)) {
                leftNavHost()
            }
            Box(modifier = Modifier
                .weight(1f)) {
                rightNavHost()
            }
            if(!placeNavRailOnLeftSide)NavRailWithSelector(
                leftPaneNavController = leftPaneNavController,
                rightPaneNavController = rightPaneNavController,
                navItems = leftPaneNavItems,
            )
        }
    }

    LaunchedEffect(isDrawerOpen) {
        if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
            "DualPaneWithNavRailAndMovableNavHostLayout()." +
                    "LaunchedEffect($isDrawerOpen): Managing drawer state.")
        isDrawerOpen?.run {
            if(isDrawerOpen) {
                if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                    "DualPaneWithNavRailAndMovableNavHostLayout()." +
                            "LaunchedEffect($isDrawerOpen): viewState demands opening drawer.")

                if(scaffoldState.drawerState.isClosed) {
                    if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                        "DualPaneWithNavRailAndMovableNavHostLayout()." +
                                "LaunchedEffect($isDrawerOpen): Drawer was closed. Opening it.")

                    scaffoldState.drawerState.open()
                    launchStateEventSetDrawerOpen(null)
//                    launchStateEvent(HomeScreenStateEvent.SetDrawerOpen(null))
                }
            } else {
                if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                    "DualPaneWithNavRailAndMovableNavHostLayout()." +
                            "LaunchedEffect($isDrawerOpen): viewState demands closing drawer.")

                if(scaffoldState.drawerState.isOpen) {
                    if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                        "DualPaneWithNavRailAndMovableNavHostLayout()." +
                                "LaunchedEffect($isDrawerOpen): Drawer was open. Closing it.")
                    scaffoldState.drawerState.close()
                    launchStateEventSetDrawerOpen(null)
//                    launchStateEvent(HomeScreenStateEvent.SetDrawerOpen(null))
                }
            }
        }

    }
    if(LOG_ME) ALog.d(TAG, "(): Recomposition end.")
}