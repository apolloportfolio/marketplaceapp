package com.aps.catemplateapp.common.framework.presentation.views.examples

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.catemplateapp.R
import com.aps.catemplateapp.common.util.ALog

@Composable
fun ExampleRecompositionImplementation() {
    ALog.d("ExampleRecompositionImplementation", "(): " +
            "Recomposed")
    var clickCount by remember { mutableStateOf(0) }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Button(
            onClick = {
                ALog.d("ExampleRecompositionImplementation", "(): " +
                                        "clickCount == $clickCount")
                clickCount++
            },
            modifier = Modifier.padding(16.dp),
        ) {
            Icon(
                painter = painterResource(R.drawable.ic_launcher),
                contentDescription = "description",
                modifier = Modifier.padding(end = 8.dp),
            )
            Text("Click Me")
        }
        Text(
            text = "Button clicked $clickCount times",
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(16.dp),
        )
    }
}

@Preview(showBackground = true)
@Composable
fun ExampleRecompositionImplementationPreview() {
    ExampleRecompositionImplementation()
}