package com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.model

class FirestoreMetaEntity(
    var count:Double = 0.0
) {
    companion object {
        val META_FIELD_NAME_COUNT = "count"
    }
}