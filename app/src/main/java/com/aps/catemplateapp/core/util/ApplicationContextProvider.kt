package com.aps.catemplateapp.core.util

import android.content.Context

interface ApplicationContextProvider {
    fun applicationContext(tag: String?): Context
}