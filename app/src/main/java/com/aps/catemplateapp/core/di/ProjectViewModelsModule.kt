package com.aps.catemplateapp.core.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.catemplateapp.common.util.DateUtil
import com.aps.catemplateapp.core.ProjectViewModelFactory
import com.aps.catemplateapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.catemplateapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.catemplateapp.core.business.domain.model.entities.ProjectUser
import com.aps.catemplateapp.core.business.domain.model.factories.UserFactory
import com.aps.catemplateapp.core.util.Entity1
import com.aps.catemplateapp.core.util.Entity1CacheDataSourceImpl
import com.aps.catemplateapp.core.util.Entity1Factory
import com.aps.catemplateapp.core.util.Entity1NetworkDataSource
import com.aps.catemplateapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.catemplateapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.aps.catemplateapp.feature03.business.interactors.ActivityEntity1DetailsInteractors
import com.aps.catemplateapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object ProjectViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    fun provideProjectViewModelFactory(
        registerLoginInteractors: RegisterLoginInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>
                >,
        activityEntityDetailsInteractors: ActivityEntity1DetailsInteractors<
                Entity1,
                Entity1CacheDataSourceImpl,
                Entity1NetworkDataSource,
                ActivityEntity1DetailsViewState<Entity1>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        entity1Factory: Entity1Factory,
        editor: SharedPreferences.Editor,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return ProjectViewModelFactory(
            registerLoginInteractors = registerLoginInteractors,
            activityEntityDetailsInteractors = activityEntityDetailsInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            sportsGameFactory = entity1Factory,
            editor = editor,
        )
    }
}