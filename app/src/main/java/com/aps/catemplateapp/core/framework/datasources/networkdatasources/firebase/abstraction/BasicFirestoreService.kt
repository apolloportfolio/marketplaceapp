package com.aps.catemplateapp.core.framework.datasources.networkdatasources.firebase.abstraction

import com.aps.catemplateapp.common.business.domain.model.EntityMapper
import com.aps.catemplateapp.common.util.UniqueID

interface BasicFirestoreService<EntityType, FirestoreEntityType, NetworkMapper: EntityMapper<FirestoreEntityType, EntityType>> {
    fun getCollectionName(): String

    fun getDeletesCollectionName(): String

    fun setEntityID(entity: EntityType, id: UniqueID)

    fun getEntityID(entity: EntityType): UniqueID?

    fun getFirestoreEntityID(entity: FirestoreEntityType): UniqueID?

    fun setFirestoreEntityID(entity: FirestoreEntityType, id: UniqueID)

    fun updateEntityTimestamp(entity: EntityType, timestamp: String)

    fun updateFirestoreEntityTimestamp(entity: FirestoreEntityType, timestamp: String)

    fun getFirestoreEntityType(): Class<FirestoreEntityType>

    fun getMetaDocumentName(): String

    fun getMetaFieldNameWithCount(): String


    suspend fun insertOrUpdateEntity(entity: EntityType): EntityType?

    suspend fun insertOrUpdateEntities(entities: List<EntityType>): List<EntityType>?

    suspend fun deleteEntity(primaryKey: UniqueID?)

    suspend fun insertDeletedEntity(entity: EntityType)

    suspend fun insertDeletedEntities(entities: List<EntityType>)

    suspend fun deleteDeletedEntity(entity: EntityType)

    suspend fun deleteAllEntities()

    suspend fun searchEntity(entity: EntityType): EntityType?

    suspend fun getAllEntities(): List<EntityType>

    suspend fun getAllDeletedEntities(): List<EntityType>

    suspend fun getEntityById(id: UniqueID): EntityType?
}