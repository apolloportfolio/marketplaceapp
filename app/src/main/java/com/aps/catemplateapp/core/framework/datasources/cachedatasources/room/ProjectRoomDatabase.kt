package com.aps.catemplateapp.core.framework.datasources.cachedatasources.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.catemplateapp.core.framework.datasources.cachedatasources.room.model.*
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceOrderDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductCategoryDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.daos.MarketplaceProductVendorDao
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceOrderCacheEntity
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductCacheEntity
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductCategoryCacheEntity
import com.aps.catemplateapp.marketplaceapp.framework.datasources.cachedatasources.room.model.MarketplaceProductVendorCacheEntity

@Database(entities = [
    UserCacheEntity::class,

    MarketplaceProductCacheEntity::class,
    MarketplaceOrderCacheEntity::class,
    MarketplaceProductCategoryCacheEntity::class,
    MarketplaceProductVendorCacheEntity::class,
], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class ProjectRoomDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao

    // Marketplace App
    abstract fun marketplaceProductDao() : MarketplaceProductDao

    abstract fun marketplaceOrderDao() : MarketplaceOrderDao

    abstract fun marketplaceProductCategoryDao() : MarketplaceProductCategoryDao

    abstract fun marketplaceProductVendorDao() : MarketplaceProductVendorDao


    companion object {
        const val DATABASE_NAME: String = "APSDB"
    }
}